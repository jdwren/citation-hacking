'====================================================================================================================================
' IMPORTANT: Re-running any of this code is not necessary unless one wants to update the values in the databases. All analyses
'    of citation patterns are conducted by using the queries in the databases (below). If you do want to
'    update the database, then this section describes how to run the code:
' 1) It is developed in VB6 and uses DAO to interface with Microsoft Access databases
' 2) Download all MEDLINE XML files into a directory (here it is D:\MEDLINE). They can be obtained from:
'       ftp://ftp.ncbi.nlm.nih.gov/pubmed/baseline (year-end files)
'       ftp://ftp.ncbi.nlm.nih.gov/pubmed/updatefiles/ (daily updates)
'       (download both sets)
' 3) The routines below should be run sequentially in order of appearance on the menu. The first
'    step is to find & extract the necessary information from the XML files and put them in tab-delimited
'    files with just the key information. These files are later subsetted and put into databases.
' 4) The most important file is the citation network file, which determines how much material there is
'    for further analysis for each author and/or journal.
' 5) For speed and disk efficiency, authors and journals are subsetted. In this study, we focus on highly-
'    published authors (>=100 papers in the citation network).
' 6) Databases populated with the data available at the time of the study (May 25, 2020) can be found on DropBox
'    at https://www.dropbox.com/sh/6phupok6fr1bs21/AADCIkB-MhKDDKwNo62JBu-Aa?dl=0
'
' Guide to the core databases:
' 1) "Citation hacking - author.mdb" (the most important)
'    a) tblListOfAuthors contains most of the numbers in Supplementary Table 2 - the stats for all authors
'    b) tblTemp - enter an author name of interest here to run any query (verify it exists in tblListOfAuthors first, though)
'    c) qry2_NSC_dist_plusJ - A histogram of non-self cites for the author in tblTemp, including journal name
'    d) qry2b_SC_dist_histogram - A histogram of self-cites for the author in tblTemp
' 2) "AuthorFrequency.mdb" - a list of all author names found, with frequency and last date of publication
' 3) "PMID_DB_CH_project.mdb" - used to summarize how many references were found and # of times each PMID was cited
'====================================================================================================================================

Private Sub mnuCreateCitationNetwork_Click()
Dim a$, b$, c$, FileName$, dbname$, fstr$, yr$, mon$, da$, NewString$, AuName$, JName$, FirstAuthor$, ISSN$, Anchor$
Dim sp$(), Refsp$(), ch As String * 1
Dim bytesread&, fsize&, BlockSize&, t&, u&, f&, sentencestart&, AuthPos&
Dim JNstart&, JNend&, AuthStart&, AuthEnd&, RefStart&, RefEnd&, PRstart&, PRend&, ISSNstart&, ISSNend&, PubYear&, YRstart&, YRend&
Dim recs&, TotalRecs&, Dfield&, textlen&, PMIDref&, refs&, TotRefs&, TotalAuth&
Dim pubdatepos&, lastPDpos&, nextPDpos&, nextabpos&, PMID&, PMIDpos&, PMIDend&, PMCID$, PMCIDpos&, PMCIDend&, UB&, UB2&, bLen&
Dim FileNum1%, FileNum2%, FileNum3%, FileNum4%, FileNum5%
Dim endflag%, loopflag%, flag%, TargetFlag%, APFlag%, FirstAuthorFlag%
Dim Rsplit$, PRsplit$
Dim fs As Variant
Dim PMID_DB As DAO.Database, tblPMID As DAO.Recordset
Set PMID_DB = OpenDatabase("E:\Citation Hacking\PMID_DB_CH_project.mdb")
Set tblPMID = PMID_DB.OpenRecordset("tblPMID")
tblPMID.Index = "PMID2"
'=======================================================================================================
'This routine creates a citation network and outputs:
'   1)PMID(published paper)-PMID(referenced paper)
'   2)PMID-author name
'   3)PMID-journal name-ISSN
'Importantly, it records the PMIDs read in, the # of refs per paper and the # of cites to
'   a paper. The next step will pare these files into only those papers that are within
'   the citation network.
'=======================================================================================================
frmProcessing.MousePointer = vbHourglass
Rsplit$ = "<ArticleId IdType=" & Chr$(34) & "pubmed" & Chr$(34) & ">"
PRsplit$ = "<PMID Version=" & Chr$(34) & "1" & Chr$(34) & ">"
Set fs = CreateObject("Scripting.FileSystemObject")
BlockSize = 60000

FileNum2 = FreeFile
Open "E:\Citation Hacking\Citation_network.txt" For Output As FileNum2
Print #FileNum2, "PMID" & vbTab & "referenced PMID"
FileNum3 = FreeFile
Open "E:\Citation Hacking\PMID_author_all.txt" For Output As FileNum3
Print #FileNum3, "PMID" & vbTab & "Author" & vbTab & "Anchor"
FileNum4 = FreeFile
Open "E:\Citation Hacking\PMID_Journal_all.txt" For Output As FileNum4
Print #FileNum4, "PMID" & vbTab & "Journal_Name" & vbTab & "ISSN"
FileNum5 = FreeFile
Open "E:\Citation Hacking\PMCID_PMID_mapping.txt" For Output As FileNum5        'not used in Citation Hacking, just for testing reference extraction
Print #FileNum5, "PMCID" & vbTab & "PMID"

For f = 1 To 1211                   'file#975 (MEDLINE2019) was the last file num of the previous build
    If f = 1 Then                   'New run - zero out PMIDs in PMID_DB on new runs
        tblPMID.MoveLast            'The PMID database is being used to track the PMIDs that have been processed
        t = tblPMID.RecordCount     'Updates to MEDLINE can result in the same abstract being processed multiple times
        tblPMID.MoveFirst
        While Not tblPMID.EOF
            tblPMID.Edit
            tblPMID!times_seen = 0
            tblPMID!Total_refs = 0
            tblPMID!Total_cites = 0
            tblPMID!PubYear = 0
            tblPMID.Update
            tblPMID.MoveNext
            u = u + 1
            If u / 100000 = Int(u / 100000) Then
                DoEvents
                DBEngine.Idle dbRefreshCache
                prog = u / t
                prog = Int(prog * 1000) / 1000
                lblProgress.Caption = "Zeroing out values in PMID_DB for full run: " & Str(prog): lblProgress.Refresh
            End If
        Wend
    End If
    t = 0: u = 0

    If f < 10 Then
        fstr = "000" & Trim$(Str(f))
    ElseIf f < 100 Then
        fstr = "00" & Trim$(Str(f))
    ElseIf f < 1000 Then
        fstr = "0" & Trim$(Str(f))
    Else
        fstr = Trim$(Str(f))
    End If
    FileName$ = "D:\MEDLINE\pubmed20n" & fstr & ".xml"
    lblProgress.Caption = "Processing File# " & fstr: lblProgress.Refresh
    fsize = FileLen(FileName$)
    FileNum1 = FreeFile
    Open FileName$ For Input As FileNum1
    bytesread = 0
   
    While Not EOF(FileNum1)
        'Grab a chunk of text representing a set of complete articles
        If bytesread + BlockSize > fsize Then
            a$ = Input(fsize - bytesread, FileNum1)
            bytesread = bytesread + Len(a$)
        Else
            a$ = Input(BlockSize, FileNum1)
            bytesread = bytesread + Len(a$)
            b$ = vbNullString: c$ = vbNullString: endflag = 0
            While endflag = 0                   'Extend block read to end of article (to avoid cutting key fields in half)
                b$ = b$ & Input(1, FileNum1)
                bytesread = bytesread + 1
                If bytesread = fsize Then endflag = 1
                If Right$(b$, 16) = "</PubmedArticle>" Then endflag = 1
                If Len(b$) > 60000 Then         'this step just to improve speed
                    c$ = c$ & b$: b$ = vbNullString
                End If
            Wend
            a$ = a$ & c$ & b$
        End If
        
        'PROCESS TEXT BLOCK
        sp$ = Split(a$, "</PubmedArticle>")                           'split the block by end of article flag
        UB = UBound(sp$)
        For t = 0 To UB - 1
            recs = recs + 1: TotalRecs = TotalRecs + 1
            refs = 0
            PMID = 0
            PMCID = vbNullString
            'get PMID
            PMIDpos = InStr(1, sp$(t), "<PMID Version=")
            If PMIDpos > 0 Then
                PMIDend = InStr(PMIDpos, sp$(t), ">")
                PMID = Val(Mid$(sp$(t), PMIDend + 1, InStr(PMIDend, sp$(t), "</PMID>") - PMIDpos - 1))
            End If
            'get PMCID (if there is one)
            PMCIDpos = InStr(1, sp$(t), "<ArticleId IdType=" & Chr$(34) & "pmc" & Chr$(34) & ">")
            If PMCIDpos > 0 Then
                PMCIDend = InStr(PMCIDpos, sp$(t), ">")
                PMCID$ = Mid$(sp$(t), PMCIDend + 1, InStr(PMCIDend, sp$(t), "</ArticleId>") - PMCIDend - 1)
                If PMCID > vbNullString And PMID > 0 Then
                    Print #FileNum5, PMCID$ & vbTab & PMID
                End If
            End If
            
            APFlag = 0                  'already processed flag
            tblPMID.Seek "=", PMID
            If tblPMID.NoMatch Then     'New PMID
                tblPMID.AddNew
                tblPMID!PMID2 = PMID
                tblPMID!times_seen = 1
                tblPMID.Update
            Else
                If tblPMID!times_seen = 0 Then
                    tblPMID.Edit
                    tblPMID!times_seen = 1
                    tblPMID.Update
                Else
                    tblPMID.Edit
                    tblPMID!times_seen = tblPMID!times_seen + 1
                    tblPMID.Update
                    If tblPMID!Total_refs = 0 And InStr(1, sp$(t), "<ReferenceList>") > 0 Then
                        APFlag = 1              'this PMID has been seen before, but did not have any references, but now it does
                    Else
                        APFlag = 2              'PMID seen and references either extracted or there are none to extract, so mark it as already processed
                    End If
                End If
            End If

            If APFlag < 2 And PMID > 0 Then     'APflag=0 means PMID not seen before, APflag=1 means seen but no references extracted
                'Get journal name
                JName$ = vbNullString
                JNstart = InStr(1, sp$(t), "<MedlineTA>")                 'Medline journal title abbrev
                If JNstart > 0 Then
                    JNstart = JNstart + 11
                    JNend = InStr(JNstart, sp$(t), "</MedlineTA>")
                    JName$ = Mid$(sp$(t), JNstart, JNend - JNstart)
                    JName$ = Trim(JName$)
                    If Len(JName$) > 50 Then JName$ = Left(JName$, 50)
                End If
                'Get ISSN
                ISSN$ = vbNullString
                ISSNstart = InStr(1, sp$(t), "<ISSN ")                 'Medline journal title abbrev
                If ISSNstart > 0 Then
                    ISSNstart = InStr(ISSNstart + 1, sp$(t), ">") + 1
                    ISSNend = InStr(ISSNstart, sp$(t), "</ISSN>")
                    ISSN$ = Mid$(sp$(t), ISSNstart, ISSNend - ISSNstart)
                    ISSN$ = Trim(ISSN$)
                End If
                'output PMID-journal if valid values found
                If PMID > 0 And JName$ <> vbNullString And APFlag = 0 Then      'APflag=1 means this journal-PMID was already output, and this pass is just to get references
                    Print #FileNum4, PMID & vbTab & JName$ & vbTab & ISSN$
                End If
                'Get publication year
                PubYear = 0
                YRstart = InStr(1, sp$(t), "<PubDate>")                 'Pub date delimiter
                If YRstart > 0 Then
                    YRstart = InStr(YRstart, sp$(t), "<Year>")          'zoom ahead to year field
                    If YRstart > 0 Then
                        YRstart = YRstart + 6
                        YRend = InStr(YRstart, sp$(t), "</Year>")
                        PubYear = Val(Mid$(sp$(t), YRstart, YRend - YRstart))
                        If PubYear > 2040 Or PubYear < 1700 Then
                            PubYear = 0
                        End If
                    End If
                End If
                
                '-----------------------------------------------------
                'In each PMID, look for the papers it cites
                '------------------------------------------------------
                refs = 0
                RefStart = InStr(1, sp$(t), "<ReferenceList>")                  'find the papers cited/referenced
                If RefStart > 0 Then
                    RefStart = RefStart + 15
                    RefEnd = InStr(RefStart, sp$(t), "</ReferenceList>")
                    b$ = Mid$(sp$(t), RefStart, RefEnd - RefStart)               'Extract the paper's reference list
                    Refsp$ = Split(b$, Rsplit$)            'split list by the beginning delimiter for PMID references
                    UB2 = UBound(Refsp$)
                    For u = 0 To UB2
                        PMIDref = 0
                        PRend = InStr(Refsp$(u), "</ArticleId>")          'end
                        If PRend > 0 And PRend < 12 Then PMIDref = Val(Mid$(Refsp$(u), 1, PRend - 1))
                        If PMIDref > 0 Then
                            tblPMID.Seek "=", PMIDref
                            If tblPMID.NoMatch Then         'cited PMID should already be there, but just in case it's not
                                tblPMID.AddNew
                                tblPMID!PMID2 = PMIDref
                                tblPMID!times_seen = 0
                                tblPMID!Total_cites = 1
                                tblPMID.Update
                            Else
                                tblPMID.Edit
                                tblPMID!Total_cites = tblPMID!Total_cites + 1
                                tblPMID.Update
                            End If
                            Print #FileNum2, PMID & vbTab & PMIDref
                            TotRefs = TotRefs + 1
                            refs = refs + 1
                        End If
                    Next u
                End If

                'For each PMID, output all authors in the list
                AuthStart = InStr(1, sp$(t), "<AuthorList")
                If AuthStart > 0 And APFlag = 0 Then                'APflag>0 means this was already done, so don't do it twice
                    AuthStart = AuthStart + 11
                    AuthEnd = InStr(AuthStart, sp$(t), "</AuthorList>")
                    b$ = Mid$(sp$(t), AuthStart, AuthEnd - AuthStart)
                    If InStr(b$, vbCr) > 0 Then b$ = Replace(b$, vbCr, " ")
                    If InStr(b$, vbLf) > 0 Then b$ = Replace(b$, vbLf, " ")
                    If InStr(b$, "  ") > 0 Then b$ = Replace(b$, "  ", " ")
                    If InStr(b$, "'") > 0 Then b$ = Replace(b$, "'", "`")
                    If InStr(b$, "Ã") > 0 Then 'replace accents
                        Call ReplaceAccents(b$)
                    End If
                    If InStr(b$, Chr$(34)) > 0 Then b$ = Replace(b$, Chr$(34), vbNullString)
                    If InStr(b$, "#38;") > 0 Then b$ = Replace(b$, "#38;", vbNullString)
                    If InStr(b$, "</CollectiveName>") > 0 Then
                        b$ = Replace(b$, "</CollectiveName>      </Author>", vbNullString)       'This is a trick to get rid of consortium names that appear at the end and count the 2nd-to-last author as last
                    End If
                    bLen = Len(b$)
                    AuName$ = vbNullString
                    FirstAuthorFlag = 0
                    For u = 1 To bLen               'ForeName includes middle initial
                        If Mid$(b$, u, 9) = "</Author>" And AuName$ <> vbNullString Then    '</ marks end of XML tag for author name field
                            If Len(AuName$) > 50 Then AuName$ = Left(AuName$, 50)
                            If FirstAuthorFlag = 0 Then
                                Anchor$ = "1"
                                FirstAuthorFlag = 1
                            Else
                                Anchor$ = "0"
                            End If
                            If InStr(u + 9, b$, "</Author>") = 0 Then   'no more authors after this one, so it should be the last (senior) author. If only one author, then this is still ok - they are both "anchor" authors
                                Anchor$ = "1"
                            End If
                            Print #FileNum3, PMID & vbTab & AuName$ & vbTab & Anchor$
                            AuName$ = vbNullString
                        End If
                        If Mid$(b$, u, 10) = "<LastName>" Then AuName$ = Mid$(b$, u + 10, InStr(u + 10, b$, "</LastName>") - (u + 10))
                        If Mid$(b$, u, 10) = "<ForeName>" Then AuName$ = AuName$ & ", " & Mid$(b$, u + 10, InStr(u + 10, b$, "</ForeName>") - (u + 10))
                    Next u
                End If
                
                tblPMID.Seek "=", PMID              'mark how many refs were found in this PMID
                If Not tblPMID.NoMatch Then
                    tblPMID.Edit
                    tblPMID!Total_refs = refs
                    If PubYear > 0 Then tblPMID!PubYear = PubYear
                    tblPMID.Update
                End If
            End If          'APflag<2       (APflag=1 means no refs found first time seen - common, for new articles)
        Next t
    Wend
    Close FileNum1
    Debug.Print "cites in file " & Str(f) & " :" & TotRefs
    TotRefs = 0
Next f
Close FileNum2
Close FileNum3
Close FileNum4
Close FileNum5

frmProcessing.MousePointer = vbDefault
End Sub
Private Sub mnuCreateAJsubsetfiles_Click()
Dim a$, b$, FileName$
Dim sp$()
Dim recs&, TotalRecs&, PMID&
Dim FileNum1%, FileNum2%
Dim PMID_DB As DAO.Database, tblPMID As DAO.Recordset
Set PMID_DB = OpenDatabase("E:\Citation Hacking\PMID_DB_CH_project.mdb")
Set tblPMID = PMID_DB.OpenRecordset("tblPMID")
tblPMID.Index = "PMID2"
frmProcessing.MousePointer = vbHourglass
'=========================================================================================================
'This routine finds the papers and journals that are part of the MEDLINE citation network
'   It uses PMID_DB_CH.mdb, created in the last step, to find papers that cite or are cited
'   The "_all" files are not really used further after this, but can be kept just in case.
'=========================================================================================================

FileNum1 = FreeFile
Open "E:\Citation Hacking\PMID_author_all.txt" For Input As FileNum1
FileNum2 = FreeFile
Open "E:\Citation Hacking\PMID_author.txt" For Output As FileNum2
Line Input #FileNum1, b$
Print #FileNum2, b$         'header
recs = 0: TotalRecs = 0
While Not EOF(FileNum1)
    Line Input #FileNum1, b$
    recs = recs + 1
    sp$ = Split(b$, vbTab)
    PMID = Val(sp$(0))
    tblPMID.Seek "=", PMID
    If Not tblPMID.NoMatch Then
        If tblPMID!Total_cites > 0 Or tblPMID!Total_refs > 0 Then
            Print #FileNum2, b$         'it's in the citation network - output it
            TotalRecs = TotalRecs + 1
        End If
    End If
Wend
Debug.Print Str(recs) & " PMID-author lines read in"
Debug.Print Str(TotalRecs) & " PMID-author lines written"
Close FileNum1
Close FileNum2

FileNum1 = FreeFile
Open "E:\Citation Hacking\PMID_journal_all.txt" For Input As FileNum1
FileNum2 = FreeFile
Open "E:\Citation Hacking\PMID_journal.txt" For Output As FileNum2
Line Input #FileNum1, b$
Print #FileNum2, b$         'header
recs = 0: TotalRecs = 0
While Not EOF(FileNum1)
    Line Input #FileNum1, b$
    recs = recs + 1
    sp$ = Split(b$, vbTab)
    PMID = Val(sp$(0))
    tblPMID.Seek "=", PMID
    If Not tblPMID.NoMatch Then
        If tblPMID!Total_cites > 0 Or tblPMID!Total_refs > 0 Then
            Print #FileNum2, b$          'it's in the citation network - output it
            TotalRecs = TotalRecs + 1
        End If
    End If
Wend
Debug.Print Str(recs) & " PMID-journal lines read in"
Debug.Print Str(TotalRecs) & " PMID-journal lines written"
Close FileNum1
Close FileNum2

frmProcessing.MousePointer = vbDefault
End Sub
Private Sub mnuCreateAuthFreqDB_Click()
Dim a$, b$, c$, FileName$, dbname$, fstr$, NewString$, AuName$, LastAuName$, Anchor$
Dim sp$(), sp2$(), Refsp$(), ch As String * 1
Dim bytesread&, fsize&, BlockSize&, t&, u&, f&, sentencestart&, AuthPos&, TApos&
Dim abstr&, AbStart&, abend&, AuthStart&, AuthEnd&, RefStart&, RefEnd&, PRstart&, PRend&, PMIDref&
Dim recs&, TotalRecs&, TotalAbs&, Dfield&, textlen&, TotalAuth&, UB&, UB2&
Dim pubdatepos&, lastPDpos&, nextPDpos&, nextabpos&, PMIDpos&, PMID&, LastPMID&, PubYR&, LastPubYR&
Dim FileNum1%, FileNum2%, FileNum3%, FileNum4%, endflag%, loopflag%, flag%, CountWordsFlag%, TargetFlag%
Dim fs As Variant
Dim AF As Variant, tblAuth As Variant, tblAmbiguous As Variant
frmProcessing.MousePointer = vbHourglass
'-----------------------------------------------------------------------------------------------------------------
'This routine creates a database of author names, including # of publications, anchor (first,last) authorships
'    and last year published. This will be used to select a subset for citation hacking analysis
'-----------------------------------------------------------------------------------------------------------------
Set AF = OpenDatabase("E:\Citation Hacking\Author\AuthorFrequency.mdb")
Set tblAuth = AF.OpenRecordset("tblAuthorFrequency")
tblAuth.Index = "Author"
Set tblAmbiguous = AF.OpenRecordset("tblAmbiguousAuthors")
tblAmbiguous.Index = "Author"
Set tblYear = AF.OpenRecordset("tblYear")
tblYear.Index = "PubDate"
Set PMID_DB = OpenDatabase("E:\Citation Hacking\PMID_DB_CH_project.mdb")    'PMID-year
Set tblPMID = PMID_DB.OpenRecordset("tblPMID")
tblPMID.Index = "PMID2"

recs = 0
FileNum3 = FreeFile
Open "E:\Citation Hacking\PMID_author.txt" For Input As FileNum3
Line Input #FileNum3, b$         'PMID-author list (within citation network)
While Not EOF(FileNum3)
    Line Input #FileNum3, b$
    recs = recs + 1
'crash routine
'While recs < 27178119
'    Line Input #FileNum3, b$
'    recs = recs + 1
'Wend
    If recs / 100000 = Int(recs / 100000) Then
        DoEvents
        DBEngine.Idle dbRefreshCache
        lblProgress.Caption = Str(recs) & " recs processed": lblProgress.Refresh
    End If
    sp$ = Split(b$, vbTab)
    PMID1 = Val(sp$(0))
    AuName$ = Trim(sp$(1))
    Anchor$ = Trim(sp$(2))
    tblPMID.Seek "=", PMID1
    If Not tblPMID.NoMatch Then         'get year of pub
        PubYR = tblPMID!PubYear
    Else
        PubYR = 0
    End If

    If Len(AuName$) > 50 Then AuName$ = Left$(AuName$, 50)
    sp2$ = Split(AuName$, ", ")  'split on comma (should only be one)
    UB2 = UBound(sp2$)
    If UB2 > 0 Then              'must be at least be two names
        'restrict to authors that have a middle initial or a hyphenated first-name, or two last names (hyphen or space)
        If ((InStr(sp2$(1), " ") > 0 Or InStr(sp2$(1), "-") > 0) Or (InStr(sp2$(0), " ") > 0 Or InStr(sp2$(0), "-") > 0)) And Len(sp2$(1)) > 3 Then
            tblYear.Seek "=", PubYR         'track ambig vs nonambig by year
            If tblYear.NoMatch Then
                tblYear.AddNew
                tblYear!PubDate = PubYR
                tblYear!NonAmbiguous = 1
            Else
                tblYear.Edit
                tblYear!NonAmbiguous = tblYear!NonAmbiguous + 1
            End If
            tblYear.Update
            tblAuth.Seek "=", AuName$
            If tblAuth.NoMatch Then
                tblAuth.AddNew
                tblAuth!Author = AuName$
                tblAuth!frequency = 1
                tblAuth!Anchor = Val(Anchor$)
                tblAuth!LastPubDate = PubYR
                tblAuth.Update
            Else
                tblAuth.Edit
                tblAuth!frequency = tblAuth!frequency + 1
                If Anchor$ = "1" Then tblAuth!Anchor = tblAuth!Anchor + 1
                If tblAuth!LastPubDate < PubYR Then tblAuth!LastPubDate = PubYR
                tblAuth.Update
            End If
        Else                        'Dump non-used names into another table, which we can examine if needed
            tblYear.Seek "=", PubYR
            If tblYear.NoMatch Then
                tblYear.AddNew
                tblYear!PubDate = PubYR
                tblYear!Ambiguous = 1
            Else
                tblYear.Edit
                tblYear!Ambiguous = tblYear!Ambiguous + 1
            End If
            tblYear.Update
            tblAmbiguous.Seek "=", AuName$
            If tblAmbiguous.NoMatch Then
                tblAmbiguous.AddNew
                tblAmbiguous!Author = AuName$
                tblAmbiguous!frequency = 1
                tblAmbiguous!Anchor = Val(Anchor$)
                tblAmbiguous!LastPubDate = PubYR
                tblAmbiguous.Update
            Else
                tblAmbiguous.Edit
                tblAmbiguous!frequency = tblAmbiguous!frequency + 1
                If Anchor$ = "1" Then tblAmbiguous!Anchor = tblAmbiguous!Anchor + 1
                If tblAmbiguous!LastPubDate < PubYR Then tblAmbiguous!LastPubDate = PubYR
                tblAmbiguous.Update
            End If
        End If
    End If
    LastAuName$ = AuName$
Wend
Debug.Print "PMID-authors processed: " & recs
Close FileNum3
frmProcessing.MousePointer = vbDefault
End Sub

Private Sub mnuAuthJournalSC_Click()
Dim PMIDkey#
Dim NumPapers&, AuthRecs&, Jrecs&
Dim querystring$
Dim FileNum1%, FileNum2%
Dim CiteHack As DAO.Database, tblAuthList As DAO.Recordset, tblTemp As DAO.Recordset, qryAuthSC As Variant
Dim CHJ As DAO.Database, tblJournalList As DAO.Recordset, tblTempJ As DAO.Recordset, qryJournalSC As Variant
frmProcessing.MousePointer = vbHourglass

'Citation Hacking database - author
Set CiteHack = OpenDatabase("E:\Citation Hacking\Author\Citation hacking - author.mdb")
Set tblAuthList = CiteHack.OpenRecordset("tblListOfAuthors")
Set tblTemp = CiteHack.OpenRecordset("tblTemp")
Set qryAuthSC = CiteHack.OpenRecordset("qry2b_Self_citations")
FileNum1 = FreeFile
Open "E:\Citation Hacking\Author\Author_Self-cites.txt" For Output As FileNum1
Print #FileNum1, "PMIDkey" & vbTab & "AuthorName"
'Citation Hacking database - journal
Set CHJ = OpenDatabase("E:\Citation Hacking\Journal\Citation hacking - Journal_master.mdb")
Set tblJournalList = CHJ.OpenRecordset("tblListOfJournals")
Set tblTempJ = CHJ.OpenRecordset("tblTemp")
FileNum2 = FreeFile
Open "E:\Citation Hacking\Journal\Journal_Self-cites.txt" For Output As FileNum2
Print #FileNum2, "PMIDkey" & vbTab & "JournalName"

tblAuthList.MoveFirst
While Not tblAuthList.EOF
    tblTemp.Edit
    tblTemp!authorname = tblAuthList!Author
    tblTemp.Update
    AuthRecs = AuthRecs + 1
    qryAuthSC.Requery
    If qryAuthSC.RecordCount > 0 Then
        qryAuthSC.MoveFirst
        While Not qryAuthSC.EOF
            Print #FileNum1, qryAuthSC!CitingPMID & qryAuthSC!PMID & vbTab & qryAuthSC!authorname
            qryAuthSC.MoveNext
        Wend
    End If
    tblAuthList.MoveNext
Wend
Close FileNum1

tblJournalList.MoveFirst
While Not tblJournalList.EOF
    tblTempJ.Edit
    tblTempJ!JournalName = tblJournalList!Journal
    tblTempJ.Update
    Jrecs = Jrecs + 1
    NumPapers = tblJournalList!frequency
    qrystring$ = "qry1a_JournalSC"
    Call Find_Suffix(NumPapers, qrystring$)     'just adds a suffix
    If qrystring$ <> vbNullString Then
        Set qryJournalSC = CHJ.OpenRecordset(qrystring$)
    End If
    
    If qryJournalSC.RecordCount > 0 Then
        qryJournalSC.MoveFirst
        While Not qryJournalSC.EOF
            Print #FileNum2, qryJournalSC!PMID1 & qryJournalSC!PMID2 & vbTab & qryJournalSC!Journal_name
            qryJournalSC.MoveNext
        Wend
    End If
    tblJournalList.MoveNext
Wend
Close FileNum2

frmProcessing.MousePointer = vbDefault
End Sub

Private Sub mnuAuthorAmbiguity_Click()
Dim a$, b$, c$, FileName$, AuName$, LastName$, Initials$, LNinitials$
Dim sp$(), sp2$()
Dim recs&, recs2&
Dim AF As Variant, tblAuth As Variant, tblAuthAmbig As Variant, qryLNAmbig As Variant
'--------------------------------------------------------------------------------------------------------------------------
'Step 1)
'    For all authors with >=100 papers, we will check how many instances of their name plus initials there are to
'    estimate potential ambiguity. Also, we will check last name abundance.
'--------------------------------------------------------------------------------------------------------------------------
frmProcessing.MousePointer = vbHourglass
Set AF = OpenDatabase("E:\Citation Hacking\Author\AuthorFrequency.mdb")
Set tblAuth = AF.OpenRecordset("tblAuthorFrequency")
tblAuth.Index = "Author"
Set tblAuthAmbig = AF.OpenRecordset("tblAmbiguousAuthors")
tblAuthAmbig.Index = "Author"

tblAuth.MoveFirst
While Not tblAuth.EOF
    If tblAuth!frequency >= 100 And tblAuth!LastPubDate >= 2017 Then
        AuName$ = tblAuth!Author
        sp$ = Split(AuName$, ", ")
        LastName$ = sp$(0)
        'first, look for the abbreviated form of the author's name
        tblAuth.Edit
        If InStr(sp$(1), " ") > 0 Then      'author has name + MI
            sp2$ = Split(sp$(1), " ")
            Initials$ = Left$(sp$(1), 1) & " " & Right$(sp2$(1), 1)
            LNinitials$ = LastName$ & ", " & Initials$
            tblAuthAmbig.Seek "=", LNinitials$
            If Not tblAuthAmbig.NoMatch Then
                tblAuth!SameNameWInitials = tblAuthAmbig!frequency
            Else
                tblAuth!SameNameWInitials = 0
            End If
        End If
        recs = 0
        Set qryLNAmbig = AF.OpenRecordset("SELECT * FROM tblAmbiguousAuthors WHERE (((tblAmbiguousAuthors.Author) Like " & Chr$(34) & LastName$ & "*" & Chr$(34) & ") AND ((tblAmbiguousAuthors.Frequency)>=5))")
        If qryLNAmbig.RecordCount > 0 Then
            qryLNAmbig.MoveLast
            recs = recs + qryLNAmbig.RecordCount
        End If
        Set qryLNAmbig = AF.OpenRecordset("SELECT * FROM tblAuthorFrequency WHERE (((tblAuthorFrequency.Author) Like " & Chr$(34) & LastName$ & "*" & Chr$(34) & ") AND ((tblAuthorFrequency.Frequency)>=5))")
        If qryLNAmbig.RecordCount > 0 Then
            qryLNAmbig.MoveLast
            recs = recs + qryLNAmbig.RecordCount - 1   'subtract one to not count the person themself
        End If
        tblAuth!LastNameFreq = recs
        tblAuth.Update
    End If
    tblAuth.MoveNext
Wend
Debug.Print "PMID-authors processed: " & recs
Debug.Print "records in new subset: " & recs2
Close FileNum2
Close FileNum3
frmProcessing.MousePointer = vbDefault

End Sub

Private Sub mnuCHAnalyzeSubset_Click()
Dim recs&
Dim NumBlocksFound&, BlockFlag&, CitesInBlock&, MaxBlockSize&, TotNSC&, TotSC&, PrevID&, CitingPMID&
Dim NSC_Hcount&, SC_Hcount&, NSCcitedonce&, SCcitedonce&, NSCcitedmany&, SCcitedmany&, MaxNSC&
Dim J_Hcount&, A_Hcount&, NumPapers&, MCJ_Hcount&, MCJpapers&, MCA_Hcount&, MCApapers&, Block_Hcount&, BlockSum&, FinalANID&, Hindex&
Dim LastJournal$, LastAuthor$, MCJ$, MCA$
Dim NSC_score!, SC_score!, BlockAvg!
Dim CiteHack As Variant, tblTemp As Variant, tblAuthList As Variant
Dim tblAuthBlocks As Variant, qryBlockIndex As Variant, tblTempBlocks As Variant
Dim qryStats As Variant, qryJournalCites As Variant, qryAuthorCites As Variant, qryAvgCites As Variant, qryCitedOnce As Variant, qryBlockCites As Variant
Dim qryNSCdist As Variant, qrySCDist As Variant, qryHindex As Variant
'------------------------------------------------------------------------------------
'This routine analyzes a subset of all well-published authors (>=100 papers) for patterns
'    of possible coercive citation. It's based upon assumptions of an approximate Zipfian distribution of the
'    frequency of NSC coming from a single paper to a single author.
'    This section compiles data on "red flag" trends and potential explanations for aberrant patterns
'    (e.g., abnormal # of cites coming from one journal or one author)
'    It overwrites the existing tblListOfAuthors
'------------------------------------------------------------------------------------
frmProcessing.MousePointer = vbHourglass

'Citation Hacking database
Set CiteHack = OpenDatabase("E:\Citation Hacking\Author\Citation hacking - author.mdb")
Set tblAuthBlocks = CiteHack.OpenRecordset("tblAuthorBlocks")
tblAuthBlocks.Index = "CNSID"
Set tblTempBlocks = CiteHack.OpenRecordset("tblTemp")
Set qryBlockIndex = CiteHack.OpenRecordset("SELECT * FROM qry1_Author_blocks_w_Jnames ORDER BY Block_Length DESC")
Set tblAuthList = CiteHack.OpenRecordset("tblListOfAuthors")
tblAuthList.Index = "Author"
Set tblTemp = CiteHack.OpenRecordset("tblTemp")
Set qryBlockCites = CiteHack.OpenRecordset("SELECT * FROM qry2_NS_citations ORDER BY ID ASC")     'ID field here is just the autonumber added during import
Set qryNSCdist = CiteHack.OpenRecordset("SELECT * FROM qry2_NSC_distribution ORDER BY CountOfCitingPMID DESC")
Set qrySCDist = CiteHack.OpenRecordset("SELECT * FROM qry2b_SC_distribution ORDER BY CountOfCitingPMID DESC")
Set qryJournalCites = CiteHack.OpenRecordset("SELECT * FROM qry2_NSC_dist_plusJ ORDER BY Journal_Name ASC, CountOfCitingPMID DESC")  'nested sort
Set qryAuthorCites = CiteHack.OpenRecordset("SELECT * FROM qry2_NSC_dist_plusAuthor ORDER BY AuthorName ASC, CountOfCitingPMID DESC")  'nested sort

tblAuthList.MoveFirst
While Not tblAuthList.EOF
    tblTemp.Edit
    tblTemp!authorname = tblAuthList!Author
    tblTemp.Update
    
    '-----------------
    'Look for block (i.e., consecutive) citations within the reference list
    'This presumes NCBI enters them sequentially, which appears to be the case
    'NOTE: Although the query to determine block index is grouped to protect against counting duplicates, the entries
    '      from the last run should be deleted for space/efficiency/crash-prevention anyway
    '-----------------
    qryBlockCites.Requery
    TotNSC = 0: NumBlocksFound = 0: BlockSum = 0: BlockAvg = 0: CitesInBlock = 1: BlockFlag = 0: MaxBlockSize = 0
    If qryBlockCites.RecordCount > 0 Then
        qryBlockCites.MoveFirst
        PrevID = qryBlockCites!ID
        TotNSC = 1
        qryBlockCites.MoveNext
        While Not qryBlockCites.EOF
            TotNSC = TotNSC + 1
            'look for clusters of citations, determined by autonumbered ID, allowing for minor gaps
            If (qryBlockCites!ID = (PrevID + 1)) Then
                CitesInBlock = CitesInBlock + 1
                CitingPMID = qryBlockCites!CitingPMID
            Else
                If CitesInBlock >= 3 Then                'saw at least 3 in a row before break in continuity
                    NumBlocksFound = NumBlocksFound + 1
                    If CitesInBlock > MaxBlockSize Then MaxBlockSize = CitesInBlock
                    tblAuthBlocks.Seek "=", qryBlockCites!ID    'We'll use the citation network ID to track and summarize blocks
                    If tblAuthBlocks.NoMatch Then
                        tblAuthBlocks.AddNew
                        tblAuthBlocks!CNSID = qryBlockCites!ID
                    Else
                        tblAuthBlocks.Edit
                    End If
                    tblAuthBlocks!Author = tblTemp!authorname
                    tblAuthBlocks!Citing_PMID = CitingPMID
                    tblAuthBlocks!Block_Length = CitesInBlock
                    tblAuthBlocks.Update
                    BlockSum = BlockSum + CitesInBlock
                End If
                CitesInBlock = 1                'since it takes 3 to make a block, the count starts at 1, not 0
                CitingPMID = 0
            End If
            PrevID = qryBlockCites!ID
            qryBlockCites.MoveNext
        Wend
        'In the event the block went up to the very end, one final check is needed
        If CitesInBlock >= 3 Then
            NumBlocksFound = NumBlocksFound + 1
            If CitesInBlock > MaxBlockSize Then MaxBlockSize = CitesInBlock
            tblAuthBlocks.Seek "=", qryBlockCites!ID
            If tblAuthBlocks.NoMatch Then
                tblAuthBlocks.AddNew
                tblAuthBlocks!CNSID = qryBlockCites!ID
            Else
                tblAuthBlocks.Edit
            End If
            tblAuthBlocks!Author = tblTemp!authorname
            tblAuthBlocks!Citing_PMID = CitingPMID
            tblAuthBlocks!Block_Length = CitesInBlock
            tblAuthBlocks.Update
            BlockSum = BlockSum + CitesInBlock
        End If
        If NumBlocksFound > 0 Then BlockAvg = Int((BlockSum / NumBlocksFound) * 10) / 10
        
        '-----------------
        'run H-index analysis on NSC, SC and # of NSC Blocks found
        'and calculate scores
        '-----------------
        tblTempBlocks.Edit
        tblTempBlocks!authorname = tblAuthList!Author
        tblTempBlocks.Update
        'actual H-index (using aggregate # of citations per paper)
        Hindex = 0
        Set qryHindex = CiteHack.OpenRecordset("qryAuthorHIndex")       'already sorted by total cites descending
        If qryHindex.RecordCount > 0 Then
            qryHindex.MoveFirst
            While Not qryHindex.EOF
                If qryHindex!Total_cites > Hindex Then
                    Hindex = Hindex + 1
                Else
                    qryHindex.MoveLast
                End If
                qryHindex.MoveNext
            Wend
        End If
        'block (sequential cites) index
        Block_Hcount = 0
        qryBlockIndex.Requery
        If qryBlockIndex.RecordCount > 0 Then
            qryBlockIndex.MoveFirst
            While Not qryBlockIndex.EOF
                If qryBlockIndex!Block_Length > Block_Hcount Then
                    Block_Hcount = Block_Hcount + 1
                Else
                    qryBlockIndex.MoveLast
                End If
                qryBlockIndex.MoveNext
            Wend
        End If
        'non-self cite index
        NSC_Hcount = 0: NSCcitedonce = 0: NSCcitedmany = 0: MaxNSC = 0
        qryNSCdist.Requery
        If qryNSCdist.RecordCount > 0 Then
            qryNSCdist.MoveFirst
            MaxNSC = qryNSCdist!CountOfCitingPMID
            While Not qryNSCdist.EOF
                If qryNSCdist!CountOfCitingPMID > NSC_Hcount Then NSC_Hcount = NSC_Hcount + 1
                If qryNSCdist!CountOfCitingPMID > 1 Then
                    NSCcitedmany = NSCcitedmany + qryNSCdist!CountOfCitingPMID
                Else
                    NSCcitedonce = NSCcitedonce + 1
                End If
                qryNSCdist.MoveNext
            Wend
        End If
        'self-cite index
        SC_Hcount = 0: SCcitedonce = 0: SCcitedmany = 0: TotSC = 0
        qrySCDist.Requery
        If qrySCDist.RecordCount > 0 Then
            qrySCDist.MoveFirst
            While Not qrySCDist.EOF
                TotSC = TotSC + qrySCDist!CountOfCitingPMID
                If qrySCDist!CountOfCitingPMID > SC_Hcount Then SC_Hcount = SC_Hcount + 1
                If qrySCDist!CountOfCitingPMID > 1 Then
                    SCcitedmany = SCcitedmany + qrySCDist!CountOfCitingPMID
                Else
                    SCcitedonce = SCcitedonce + 1
                End If
                qrySCDist.MoveNext
            Wend
        End If
        'head size (sum cites>1) to tail len (cites=1) ratio
        NSC_score = Int(((NSCcitedmany + 1) / (NSCcitedonce + 1)) * 100) / 100   'add pseudocount (some people never self-cite only once)
        SC_score = Int(((SCcitedmany + 1) / (SCcitedonce + 1)) * 100) / 100
        '------------------------------------
        'look for journal NSC bias - H-index
        '------------------------------------
        qryJournalCites.Requery
        J_Hcount = 0:  LastJournal = vbNullString:  NumPapers = 0
        MCJ_Hcount = 0: MCJ = vbNullString: MCJpapers = 0
        If qryJournalCites.RecordCount > 0 Then
            qryJournalCites.MoveFirst
            While Not qryJournalCites.EOF                           'nested sort - journals then cites (desc)
                If LastJournal <> qryJournalCites!Journal_name Then 'new journal name detected
                    If J_Hcount > MCJ_Hcount Then           'higher index than previous journals
                        MCJ = LastJournal
                        MCJ_Hcount = J_Hcount
                        MCJpapers = NumPapers
                    End If
                    J_Hcount = 1                            'reset counters
                    LastJournal = qryJournalCites!Journal_name
                    NumPapers = 0
                Else
                    If qryJournalCites!CountOfCitingPMID > J_Hcount Then J_Hcount = J_Hcount + 1
                End If
                NumPapers = NumPapers + 1
                qryJournalCites.MoveNext
            Wend
        End If
        '-----------------------------------------------------------------------------------------------------------------
        'look for author NSC bias - H-index (note: this will only detect highly citing authors *within the author subset*)
        '-----------------------------------------------------------------------------------------------------------------
        qryAuthorCites.Requery
        A_Hcount = 0:  LastAuthor = vbNullString:  NumPapers = 0
        MCA_Hcount = 0: MCA = vbNullString: MCApapers = 0
        If qryAuthorCites.RecordCount > 0 Then
            qryAuthorCites.MoveFirst
            While Not qryAuthorCites.EOF                            'nested sort - author names then cites (desc)
                If LastAuthor <> qryAuthorCites!authorname Then     'new author name detected
                    If A_Hcount > MCA_Hcount Then                   'higher index than previous authors
                        MCA = LastAuthor
                        MCA_Hcount = A_Hcount
                        MCApapers = NumPapers
                    End If
                    A_Hcount = 1                            'reset counters
                    LastAuthor = qryAuthorCites!authorname
                    NumPapers = 0
                Else
                    If qryAuthorCites!CountOfCitingPMID > A_Hcount Then A_Hcount = A_Hcount + 1
                End If
                NumPapers = NumPapers + 1
                qryAuthorCites.MoveNext
            Wend
        End If
        'Store in author database
        tblAuthList.Edit
        tblAuthList!NSC_score = NSC_score
        tblAuthList!NSC_index = NSC_Hcount
        tblAuthList!SC_score = SC_score
        tblAuthList!SC_index = SC_Hcount
        tblAuthList!MaxNSC = MaxNSC
        If Len(MCJ$) > 50 Then MCJ = Left$(MCJ, 50)
        tblAuthList!MostCitingJournal = MCJ
        tblAuthList!MCJ_NumPapers = MCJpapers
        tblAuthList!MCJ_Index = MCJ_Hcount
        tblAuthList!MostCitingAuthor = MCA
        tblAuthList!MCA_NumPapers = MCApapers
        tblAuthList!MCA_Index = MCA_Hcount
        tblAuthList!TotNSC = TotNSC
        tblAuthList!TotSC = TotSC
        tblAuthList!NumBlocks = NumBlocksFound
        tblAuthList!SumOfAllBlocks = BlockSum
        tblAuthList!MaxBlockSize = MaxBlockSize
        tblAuthList!Block_index = Block_Hcount
        tblAuthList!Hindex = Hindex
        tblAuthList.Update
        recs = recs + 1
        lblProgress.Caption = Str(recs) & " authors processed": lblProgress.Refresh
    End If
    tblAuthList.MoveNext
Wend
frmProcessing.MousePointer = vbDefault
Debug.Print "authors processed: " & Str(recs)
End Sub

Private Sub mnuCreateAuthSubset_Click()
Dim a$, b$, c$, FileName$, AuName$, LastRec$, LastRecExt$
Dim sp$()
Dim recs&, recs2&, recs3&
Dim FileNum1%, FileNum2%, FileNum3%
Dim AF As Variant, tblAuth As Variant
'--------------------------------------------------------------------------------------------------------------------------
'Step 2)
'For statistical power and database size manageability, we will create a subset from the list of all MEDLINE authors.
'    We will output only authors with >=100 papers, and most recent paper published within last 2 years (2017).
'--------------------------------------------------------------------------------------------------------------------------
frmProcessing.MousePointer = vbHourglass
Set AF = OpenDatabase("E:\Citation Hacking\Author\AuthorFrequency.mdb")
Set tblAuth = AF.OpenRecordset("tblAuthorFrequency")
tblAuth.Index = "Author"

FileNum3 = FreeFile
Open "E:\Citation Hacking\PMID_author.txt" For Input As FileNum3
FileNum2 = FreeFile
Open "E:\Citation Hacking\Author\PMID_author_subset.txt" For Output As FileNum2     'new subset list
FileNum1 = FreeFile
Open "E:\Citation Hacking\PMID_author_extended_subset.txt" For Output As FileNum1     'new subset list
Line Input #FileNum3, b$        'PMID-author list
While Not EOF(FileNum3)
    Line Input #FileNum3, b$
    recs = recs + 1
    sp$ = Split(b$, vbTab)
    AuName$ = Trim(sp$(1))
    tblAuth.Seek "=", AuName$               'The author name is in the non-ambiguous list
    If Not tblAuth.NoMatch Then
        If tblAuth!frequency >= 100 And tblAuth!LastPubDate >= 2017 Then
            If b$ <> LastRec$ Then          'sometimes, e.g., with consortia, an author will be listed twice. This check just prevents immediate duplicates
                Print #FileNum2, b$
                LastRec$ = b$
                recs2 = recs2 + 1
            End If
        End If
        If tblAuth!frequency >= 10 And tblAuth!LastPubDate >= 2010 Then     'extended subset for Most Citing Author analysis
            If b$ <> LastRecExt$ Then
                Print #FileNum1, b$
                LastRecExt$ = b$
                recs3 = recs3 + 1
            End If
        End If
    End If
Wend
Debug.Print "PMID-authors processed    : " & recs
Debug.Print "records in 100+ subset    : " & recs2
Debug.Print "records in extended subset: " & recs3
Close FileNum1
Close FileNum2
Close FileNum3
frmProcessing.MousePointer = vbDefault
End Sub
Private Sub mnuCreateCitationSubset_Click()
Dim a$, b$, c$, FileName$, AuName$
Dim sp$()
Dim recs&, recs2&, PMID1&, PMID2&
Dim FileNum1%, FileNum2%, FileNum3%, OutputFlag%
Dim ch As Variant, tblAuth As Variant
'------------------------------------------------------------------------------------
'Step 3)
'For the active authors with >=100 papers, we need to also get the subset of citations
'    to their papers.
'Prior to running, in "Citation hacking - author.mdb":
'   1) Import the subset of selected author names from the previous step into the list of authors
'   2) Import PMID_author_subset from the previous step into tblPMID_author_subset
'------------------------------------------------------------------------------------
frmProcessing.MousePointer = vbHourglass
Set ch = OpenDatabase("E:\Citation Hacking\Author\Citation hacking - author.mdb")
Set tblAuth = ch.OpenRecordset("tblPMID_author_subset")
tblAuth.Index = "PMID"

recs = 0
FileNum1 = FreeFile
Open "E:\Citation Hacking\Citation_network.txt" For Input As FileNum1
FileNum2 = FreeFile
Open "E:\Citation Hacking\Author\Citation_network_subset.txt" For Output As FileNum2     'new subset list
Print #FileNum2, "PMID" & vbTab & "PMID_cited" & vbTab & "ID"
Line Input #FileNum1, b$        'PMID-PMID list
While Not EOF(FileNum1)
    Line Input #FileNum1, b$
    recs = recs + 1
    sp$ = Split(b$, vbTab)
    PMID1 = Val(sp$(0))         'paper citing
    PMID2 = Val(sp$(1))         'paper being cited
    OutputFlag = 0
    tblAuth.Seek "=", PMID2     'does the cited paper belong to the subset of authors being analyzed?
    If Not tblAuth.NoMatch Then
        OutputFlag = 1
    End If
    If OutputFlag = 1 Then      'output both PMIDs plus the record# (so we can later track blocks of citations)
        Print #FileNum2, b$ & vbTab & Trim(Str(recs))
        recs2 = recs2 + 1
    End If
Wend
Debug.Print "Citations read: " & recs
Debug.Print "Citations put into subset: " & recs2
Close FileNum2
Close FileNum1
frmProcessing.MousePointer = vbDefault
End Sub

Public Sub ReplaceAccents(b$)
If InStr(b$, "Ã¡") > 0 Then b$ = Replace(b$, "Ã¡", "a")
If InStr(b$, "Ã‚") > 0 Then b$ = Replace(b$, "Ã‚", "A")
If InStr(b$, "Ã„") > 0 Then b$ = Replace(b$, "Ã„", "A")
If InStr(b$, "Ã¢") > 0 Then b$ = Replace(b$, "Ã¢", "a")
If InStr(b$, "Ã£") > 0 Then b$ = Replace(b$, "Ã£", "a")
If InStr(b$, "Ã¤") > 0 Then b$ = Replace(b$, "Ã¤", "a")
If InStr(b$, "Ã¥") > 0 Then b$ = Replace(b$, "Ã¥", "a")
If InStr(b$, "Ã ") > 0 Then b$ = Replace(b$, "Ã ", "a")
If InStr(b$, "Ã€") > 0 Then b$ = Replace(b$, "Ã€", "A")
If InStr(b$, "Ã…") > 0 Then b$ = Replace(b$, "Ã…", "A")
If InStr(b$, "Ãƒ") > 0 Then b$ = Replace(b$, "Ãƒ", "A")
If InStr(b$, "Ã¦") > 0 Then b$ = Replace(b$, "Ã¦", "ae")
If InStr(b$, "Ã§") > 0 Then b$ = Replace(b$, "Ã§", "c")
If InStr(b$, "Ã‡") > 0 Then b$ = Replace(b$, "Ã‡", "C")
If InStr(b$, "Ãˆ") > 0 Then b$ = Replace(b$, "Ãˆ", "E")
If InStr(b$, "Ã¨") > 0 Then b$ = Replace(b$, "Ã¨", "e")
If InStr(b$, "Ã‹") > 0 Then b$ = Replace(b$, "Ã‹", "E")
If InStr(b$, "Ã«") > 0 Then b$ = Replace(b$, "Ã«", "e")
If InStr(b$, "Ã©") > 0 Then b$ = Replace(b$, "Ã©", "e")
If InStr(b$, "Ã‰") > 0 Then b$ = Replace(b$, "Ã‰", "E")
If InStr(b$, "Ãª") > 0 Then b$ = Replace(b$, "Ãª", "e")
If InStr(b$, "ÃŠ") > 0 Then b$ = Replace(b$, "ÃŠ", "E")
If InStr(b$, "Ã­") > 0 Then b$ = Replace(b$, "Ã­", "i")
If InStr(b$, "Ã¯") > 0 Then b$ = Replace(b$, "Ã¯", "i")
If InStr(b$, "Ã¬") > 0 Then b$ = Replace(b$, "Ã¬", "i")
If InStr(b$, "Ã®") > 0 Then b$ = Replace(b$, "Ã®", "i")
If InStr(b$, "ÃŒ") > 0 Then b$ = Replace(b$, "ÃŒ", "I")
If InStr(b$, "ÃŽ") > 0 Then b$ = Replace(b$, "ÃŽ", "I")
If InStr(b$, "Å‚") > 0 Then b$ = Replace(b$, "Å‚", "l")
If InStr(b$, "Ã‘") > 0 Then b$ = Replace(b$, "Ã‘", "N")
If InStr(b$, "Ã±") > 0 Then b$ = Replace(b$, "Ã±", "n")
If InStr(b$, "Ã´") > 0 Then b$ = Replace(b$, "Ã´", "o")
If InStr(b$, "Ã¸") > 0 Then b$ = Replace(b$, "Ã¸", "o")
If InStr(b$, "Ã°") > 0 Then b$ = Replace(b$, "Ã°", "o")
If InStr(b$, "Ãµ") > 0 Then b$ = Replace(b$, "Ãµ", "o")
If InStr(b$, "Ã¶") > 0 Then b$ = Replace(b$, "Ã¶", "o")
If InStr(b$, "Ã²") > 0 Then b$ = Replace(b$, "Ã²", "o")
If InStr(b$, "Ã³") > 0 Then b$ = Replace(b$, "Ã³", "o")
If InStr(b$, "Ã•") > 0 Then b$ = Replace(b$, "Ã•", "O")
If InStr(b$, "Ã–") > 0 Then b$ = Replace(b$, "Ã–", "O")
If InStr(b$, "Ã’") > 0 Then b$ = Replace(b$, "Ã’", "O")
If InStr(b$, "Ã“") > 0 Then b$ = Replace(b$, "Ã“", "O")
If InStr(b$, "Ã”") > 0 Then b$ = Replace(b$, "Ã”", "O")
If InStr(b$, "Ã~") > 0 Then b$ = Replace(b$, "Ã~", "O")
If InStr(b$, "ÃŸ") > 0 Then b$ = Replace(b$, "ÃŸ", "S")
If InStr(b$, "Ã›") > 0 Then b$ = Replace(b$, "Ã›", "U")
If InStr(b$, "Ã»") > 0 Then b$ = Replace(b$, "Ã»", "u")
If InStr(b$, "Ã¼") > 0 Then b$ = Replace(b$, "Ã¼", "u")
If InStr(b$, "Ã¹") > 0 Then b$ = Replace(b$, "Ã¹", "u")
If InStr(b$, "Ãº") > 0 Then b$ = Replace(b$, "Ãº", "u")
If InStr(b$, "Ãœ") > 0 Then b$ = Replace(b$, "Ãœ", "U")
If InStr(b$, "Ãš") > 0 Then b$ = Replace(b$, "Ãš", "U")
If InStr(b$, "Ã™") > 0 Then b$ = Replace(b$, "Ã™", "U")
If InStr(b$, "Ã—") > 0 Then b$ = Replace(b$, "Ã—", "x")
If InStr(b$, "Ã¿") > 0 Then b$ = Replace(b$, "Ã¿", "y")
If InStr(b$, "Ã½") > 0 Then b$ = Replace(b$, "Ã½", "y")
End Sub
Private Sub btnCitationHackingPubMed_Click()
Dim a$, b$, c$, FileName$, dbname$, fstr$, acro$, def$, yr$, mon$, da$, tit$, NewString$, dept$, AuName$, LCname$
Dim sp$(), Refsp$(), ch As String * 1
Dim bytesread&, fsize&, BlockSize&, t&, u&, f&, sentencestart&, AuthPos&, TApos&
Dim Skey#
Dim abstr&, AbStart&, abend&, AuthStart&, AuthEnd&, OrgStart&, OrgEnd&, RefStart&, RefEnd&, PRstart&, PRend&, PMIDref&
Dim recs&, TotalRecs&, TotalAbs&, Dfield&, textlen&, TotalAuth&
Dim pubdatepos&, lastPDpos&, nextPDpos&, nextabpos&, PMIDpos&, PMID&, LastPMID&
Dim FileNum1%, FileNum2%, FileNum3%, FileNum4%, endflag%, loopflag%, flag%, CountWordsFlag%, TargetFlag%
Dim fs As Variant
Dim CiteHack As Variant, tblCitePMID As Variant, tblPMIDJournal As Variant, tblPMIDref As Variant, tblPMIDauth As Variant, tblAuthList As Variant
'------------------------------------------------------------------------------------
'This routine compiles the data for analysis of whether or not a target author
'    of interest may be engaged in citation hacking
'------------------------------------------------------------------------------------
frmProcessing.MousePointer = vbHourglass

Set CiteHack = OpenDatabase("C:\Program Files\Microsoft Visual Studio\VB98\PubQC\Citation hacking.mdb")
Set tblPMIDauth = CiteHack.OpenRecordset("tblPMID_author")
tblPMIDauth.Index = "PMID"
Set tblPMIDref = CiteHack.OpenRecordset("tblPMID_references")
tblPMIDref.Index = "key"
Set tblCitePMID = CiteHack.OpenRecordset("tblCitations_PMID")
tblCitePMID.Index = "key"
Set tblAuthRef = CiteHack.OpenRecordset("tblAuthorsReferenced")
tblAuthRef.Index = "PMID"
Set tblPMIDJournal = CiteHack.OpenRecordset("tblPMID_Journal")
tblPMIDJournal.Index = "PMID"
Set tblAuthList = CiteHack.OpenRecordset("tblListOfAuthors")
tblAuthList.Index = "Author"

'------------------------------------------------------------------------------------
'First, find all the PMIDs of papers the target author has published
'------------------------------------------------------------------------------------
recs = 0
FileNum3 = FreeFile
Open "E:\PMID_author.txt" For Input As FileNum3
Line Input #FileNum3, b$        'PMID-author list
While Not EOF(FileNum3)
    Line Input #FileNum3, b$
    recs = recs + 1
    sp$ = Split(b$, vbTab)
    PMID1 = Val(sp$(0))
    AuName$ = sp$(1)
'    LCname$ = LCase(AuName$)
    If PMID1 <> LastPMID Then    'new PMID, keep track of # of authors
        AuthorPos = 1: TotalAuth = 1
        LastPMID = PMID1
    Else
        AuthorPos = AuthorPos + 1: TotalAuth = TotalAuth + 1
    End If
    
    tblAuthList.Seek "=", AuName$
    If Not tblAuthList.NoMatch Then
'    If AuName$ = "Chou, Kuo-Chen" Then
        tblPMIDauth.Seek "=", PMID1
        If tblPMIDauth.NoMatch Then              'since PubMed also contains update records, check to see that it's not already there
            tblPMIDauth.AddNew
            tblPMIDauth!PMID = PMID1
            tblPMIDauth!authorname = AuName$        'Target author
            tblPMIDauth!AuthorPos = AuthorPos       'position on the paper
            TApos = AuthorPos
            Line Input #FileNum3, b$
            sp$ = Split(b$, vbTab)
            PMID1 = Val(sp$(0))
            While PMID1 = LastPMID             'keep going until the end of the author list is reached to know his position
                AuthorPos = AuthorPos + 1
                TotalAuth = TotalAuth + 1
                Line Input #FileNum3, b$
                sp$ = Split(b$, vbTab)
                PMID1 = Val(sp$(0))
            Wend
            tblPMIDauth!TotalAuthors = TotalAuth   'position on the paper
            If TApos = 1 Or TApos = TotalAuth Then
                tblPMIDauth!Anchor_author = True
            Else
                tblPMIDauth!Anchor_author = False
            End If
            tblPMIDauth.Update
        End If
        LastPMID = PMID1
    End If
Wend
Debug.Print "PMID-authors processed: " & recs
Close FileNum3

'------------------------------------------------------------------------------------------------
'Now that we have the PMIDs of the target author's papers, find:
'    1) PMIDs of papers referenced by the target author     (potential citation ring buddies)
'    2) PMIDs of papers that cite target author             (potentially abused authors)
'------------------------------------------------------------------------------------------------
recs = 0
FileNum2 = FreeFile
Open "E:\Citation_network.txt" For Input As FileNum2
Line Input #FileNum2, b$        'PMID-PMID citation list
While Not EOF(FileNum2)
    Line Input #FileNum2, b$
    recs = recs + 1
    sp$ = Split(b$, vbTab)
    PMID1 = Val(sp$(0))
    PMID2 = Val(sp$(1))
    
    tblPMIDauth.Seek "=", PMID2             'is target author being cited by someone else?
    If Not tblPMIDauth.NoMatch Then         'yes
        Skey = Val(sp$(0) & sp$(1))         'unique key to see if this PMID pair has already been added
        tblCitePMID.Seek "=", Skey
        If tblCitePMID.NoMatch Then
            tblCitePMID.AddNew
            tblCitePMID!PMID = PMID1
            tblCitePMID!PMID_cited = PMID2
            tblCitePMID!Key = Skey
            tblCitePMID.Update
        End If
    End If
    tblPMIDauth.Seek "=", PMID1             'is target author referencing someone?
    If Not tblPMIDauth.NoMatch Then         'yes
        Skey = Val(sp$(0) & sp$(1))         'unique key to see if this PMID pair has already been added
        tblPMIDref.Seek "=", Skey
        If tblPMIDref.NoMatch Then
            tblPMIDref.AddNew
            tblPMIDref!PMID = PMID1
            tblPMIDref!PMID_referenced = PMID2
            tblPMIDref!Key = Skey
            tblPMIDref.Update
        End If
    End If
Wend
Debug.Print "citation links processed: " & recs
Close FileNum2

'------------------------------------------------------------------------------------------------
'Now that we have the PMIDs of references & citations, find:
'    1) Author names referenced by the target author     (potential citation ring buddies)
'    (future) 2) co-author names                         (ghost authorship?)
'------------------------------------------------------------------------------------------------
recs = 0
FileNum2 = FreeFile
Open "E:\PMID_author.txt" For Input As FileNum2
Line Input #FileNum2, b$
tblPMIDref.Index = "PMID_referenced"
While Not EOF(FileNum2)
    Line Input #FileNum2, b$
    recs = recs + 1
    sp$ = Split(b$, vbTab)
    PMID1 = Val(sp$(0))
    AuName$ = sp$(1)
    If PMID1 <> LastPMID Then                  'new PMID
        tblPMIDref.Seek "=", PMID1             'is this a paper the target author references?
        If Not tblPMIDref.NoMatch Then         'yes
            tblAuthRef.Seek "=", PMID1
            If tblAuthRef.NoMatch Then         'since PubMed also contains update records, check to see that it's not already there
                LastPMID = PMID1
                AuthorPos = 1: TotalAuth = 1        'start subloop to add all authors for this PMID
                While PMID1 = LastPMID
                    tblAuthRef.AddNew                  'catalog all authors the target author references (so we can check for citation ring buddies)
                    tblAuthRef!PMID = PMID1            'Target author's paper that references this author
                    tblAuthRef!Author = AuName$        'The author being referenced
                    tblAuthRef!AuthorPos = AuthorPos   'position on the paper
                    tblAuthRef.Update
                    Line Input #FileNum2, b$
                    sp$ = Split(b$, vbTab)
                    PMID1 = Val(sp$(0))
                    AuName$ = sp$(1)
                    AuthorPos = AuthorPos + 1
                    TotalAuth = TotalAuth + 1
                Wend
            End If
        End If
        LastPMID = PMID1
    End If
Wend
Close FileNum2
Debug.Print "PMIDs processed: " & Str(recs)

'------------------------------------------------------------------------------------------------
'add journal names (non-critical step - can be skipped)
'------------------------------------------------------------------------------------------------
recs = 0
FileNum2 = FreeFile
Open "E:\PMID_Journal.txt" For Input As FileNum2
Line Input #FileNum2, b$        'PMID-journal name list
While Not EOF(FileNum2)
    Line Input #FileNum2, b$
    recs = recs + 1
    sp$ = Split(b$, vbTab)
    PMID1 = Val(sp$(0))
    tblPMIDauth.Seek "=", PMID1             'is this PMID in the target author's list?
    If Not tblPMIDauth.NoMatch Then         'yes
        JName$ = sp$(1)
        tblPMIDJournal.Seek "=", PMID1
        If tblPMIDJournal.NoMatch Then
            tblPMIDJournal.AddNew
            tblPMIDJournal!PMID = PMID1
            tblPMIDJournal!Journal = JName$
            tblPMIDJournal.Update
        End If
    End If
Wend
Debug.Print "PMID-journal names processed: " & recs
Close FileNum2

frmProcessing.MousePointer = vbDefault
End Sub

Private Sub mnuCreateJFDB_Click()
Dim a$, b$, c$, FileName$, JName$, LastJname$, AuthLN$, LastAuthLN$, ISSN$
Dim sp$(), namesp$()
Dim recs&, TotalRecs&, UB&, PMID1&, PMID2&, LastPMID&, TotAuths&, Alpha&, CiteTo&, CiteFrom&
Dim FileNum1%, FileNum2%, FileNum3%
Dim AF As Variant, tblJournal As Variant, tblJISSN As Variant
Dim PJ As Variant, tblPJ As Variant
Dim PMID_DB As Variant, tblPMID As Variant
frmProcessing.MousePointer = vbHourglass
'---------------------------------------------------------------------------------------------------
' Prior to running: Import the PMID-journal list into PMID_Journal.mdb so
' that cites to and from can be calculated as well as journals that have
' alphabetical cite lists identified
'---------------------------------------------------------------------------------------------------
'This routine compiles journal-level data within the citation network: # papers published,
'    # cites to and from, the most recent year of the cites, and ISSNs associated with the journal.
'    It's used to later select journals for citation hacking analysis
'---------------------------------------------------------------------------------------------------

Set AF = OpenDatabase("E:\Citation Hacking\Journal\JournalFrequency.mdb")
Set tblJournal = AF.OpenRecordset("tblJournalFrequency")
tblJournal.Index = "Journal"
Set tblJISSN = AF.OpenRecordset("tblISSN_Jname")
tblJISSN.Index = "JISSN"
'---------------------------------------------------------------------------------------
'Tally total # of papers (with citations) by journal, and map ISSNs to Journal names
'---------------------------------------------------------------------------------------
recs = 0
FileNum1 = FreeFile
Open "E:\Citation Hacking\PMID_Journal.txt" For Input As FileNum1
Line Input #FileNum1, b$        'PMID-Journal list
While Not EOF(FileNum1)
    Line Input #FileNum1, b$
    recs = recs + 1
    If recs / 100000 = Int(recs / 100000) Then
        DoEvents
        DBEngine.Idle dbRefreshCache
        lblProgress.Caption = Str(recs / 1000) & "K recs processed": lblProgress.Refresh
    End If
    sp$ = Split(b$, vbTab)
    PMID1 = Val(sp$(0))
    JName$ = Trim(sp$(1))
    ISSN$ = Trim(sp$(2))
    If Len(JName$) > 50 Then JName$ = Left$(JName$, 50)
    tblJournal.Seek "=", JName$
    If tblJournal.NoMatch Then
        tblJournal.AddNew
        tblJournal.Journal = JName$
        tblJournal!papers = 1
        tblJournal!FirstCiteTo = 9999
        tblJournal!FirstCiteFrom = 9999
        tblJournal!LastCiteTo = 0
        tblJournal!LastCiteFrom = 0
        tblJournal.Update
    Else
        tblJournal.Edit
        tblJournal!papers = tblJournal!papers + 1
        tblJournal.Update
    End If
    tblJISSN.Seek "=", JName$ & ISSN$             'map each unique journal-ISSN combo (J's may split, change names, etc)
    If tblJISSN.NoMatch Then
        tblJISSN.AddNew
        tblJISSN.JISSN = JName$ & ISSN$
        tblJISSN.Journal = JName$
        tblJISSN.ISSN = ISSN$
        tblJISSN.Update
    End If
Wend
Debug.Print "PMID-journals processed: " & recs
Close FileNum1

'------------------------------------------------------------------------------------
'tally total # of cites to and from the journal
'------------------------------------------------------------------------------------
Set PJ = OpenDatabase("E:\Citation Hacking\PMID_Journal.mdb")               'PMID-journal
Set tblPJ = PJ.OpenRecordset("tblPMID_Journal")
tblPJ.Index = "PMID"
Set PMID_DB = OpenDatabase("E:\Citation Hacking\PMID_DB_CH_project.mdb")    'PMID-year
Set tblPMID = PMID_DB.OpenRecordset("tblPMID")
tblPMID.Index = "PMID2"
recs = 0
FileNum1 = FreeFile
Open "E:\Citation Hacking\Citation_network.txt" For Input As FileNum1
Line Input #FileNum1, b$        'PMID-PMID list
While Not EOF(FileNum1)
    Line Input #FileNum1, b$
    recs = recs + 1
    If recs / 100000 = Int(recs / 100000) Then
        DoEvents
        DBEngine.Idle dbRefreshCache
        lblProgress.Caption = Str(recs / 1000) & "K recs processed": lblProgress.Refresh
    End If

    sp$ = Split(b$, vbTab)
    PMID1 = Val(sp$(0))         'paper citing
    PMID2 = Val(sp$(1))         'paper being cited
    CiteFrom = 0: CiteTo = 0
    tblPMID.Seek "=", PMID1
    If Not tblPMID.NoMatch Then
        CiteFrom = tblPMID!PubYear
    End If
    tblPMID.Seek "=", PMID2
    If Not tblPMID.NoMatch Then
        If Not IsNull(tblPMID!PubYear) Then
            CiteTo = tblPMID!PubYear
        End If
    End If
    
    tblPJ.Seek "=", PMID1
    If Not tblPJ.NoMatch Then
        tblJournal.Seek "=", tblPJ!Journal_name
        If Not tblJournal.NoMatch Then
            tblJournal.Edit
            tblJournal!cites_from = tblJournal!cites_from + 1
            If CiteFrom > 0 Then
                If CiteFrom < tblJournal!FirstCiteFrom Then tblJournal!FirstCiteFrom = CiteFrom
                If CiteFrom > tblJournal!LastCiteFrom Then tblJournal!LastCiteFrom = CiteFrom
            End If
            tblJournal.Update
        End If
    End If
    tblPJ.Seek "=", PMID2
    If Not tblPJ.NoMatch Then
        tblJournal.Seek "=", tblPJ!Journal_name
        If Not tblJournal.NoMatch Then
            tblJournal.Edit
            tblJournal!Cites_to = tblJournal!Cites_to + 1
            If CiteTo > 0 Then
                If CiteTo < tblJournal!FirstCiteTo Then tblJournal!FirstCiteTo = CiteTo
                If CiteTo > tblJournal!LastCiteTo Then tblJournal!LastCiteTo = CiteTo
            End If
            tblJournal.Update
        End If
    End If
Wend
Debug.Print "Cites to journals processed: " & recs
Close FileNum1

frmProcessing.MousePointer = vbDefault
End Sub
Public Sub Find_Suffix(NumPapers&, qrystring$)
'based on the # of papers, choose the appropriate subset DB to query
    If NumPapers <= 200 Then qrystring = vbNullString
    If NumPapers > 200 And NumPapers <= 2000 Then qrystring$ = qrystring$
    If NumPapers > 2000 And NumPapers <= 7000 Then qrystring$ = qrystring$ & "_1"
    If NumPapers > 7000 And NumPapers <= 21000 Then qrystring$ = qrystring$ & "_2"
    If NumPapers >= 21000 Then qrystring$ = qrystring$ & "_3"
End Sub

Private Sub mnuAuthCiteHistogram_Click()
Dim ProcessFlag%
Dim LogFreq!, PredictedFreq!, Deviation!, PCC!
Dim xAxis#(), yAxis#()
Dim AuthName$
Dim MaxFlag%, FileNum1%
Dim AuthRecs&, NumCites&, MaxNumCites&, Freq&, MinFreq&, PrevFreq&
Dim Hindex&, Hindex_NSC&, Hindex_SC&
Dim CiteHack As DAO.Database, CHZipf As DAO.Database, tblAuthList As DAO.Recordset, tblTemp As DAO.Recordset
Dim tblAuthNSC As DAO.Recordset, tblAuthSC As DAO.Recordset, tblAuthNSCZipf As DAO.Recordset, tblAuthSCZipf As DAO.Recordset
Dim tblAuthNSC_nonMCA As DAO.Recordset, tblAuthNSC_noMegaRev As DAO.Recordset
Dim qryAuthNSC As Variant, qryAuthSC As Variant, qryHindex As Variant, qryHindexHist As Variant, qrySCpct As Variant
Dim qryAuthNSC_nonMCA As Variant, qryAuthNSC_noMegaRev As Variant
frmProcessing.MousePointer = vbHourglass
'-----------------------------------------------------------------------------------
'this routine creates some variants of existing tables/queries (e.g., all non-self cites that do not come from
'  mega-reviews, or without NSC from someone's most citing author (MCA))
'NOTE: delete previous values if running again
'-----------------------------------------------------------------------------------
'Citation Hacking database - author
Set CiteHack = OpenDatabase("E:\Citation Hacking\Author\Citation hacking - author.mdb")
Set tblAuthList = CiteHack.OpenRecordset("tblListOfAuthors")
Set tblTemp = CiteHack.OpenRecordset("tblTemp")
Set qryAuthNSC = CiteHack.OpenRecordset("qry2_NSC_dist_histogram")        'sorted ascending
Set qryAuthNSC_nonMCA = CiteHack.OpenRecordset("qry2_NSC_dist_hist_nonMCA")             'sorted ascending
Set qryAuthNSC_noMegaRev = CiteHack.OpenRecordset("qry2_NSC_dist_hist_noMegaRev")       'sorted ascending
Set qryAuthSC = CiteHack.OpenRecordset("qry2b_SC_dist_histogram")         'sorted ascending
Set qryHindexHist = CiteHack.OpenRecordset("qryAuthorHindexHist_NSC")     'sorted ascending
Set qrySCpct = CiteHack.OpenRecordset("qryAuthorOutgoingSCNSC_sum")

'Zipf trends and experiments DB
Set CHZipf = OpenDatabase("E:\Citation Hacking\Author\Citation hacking - author Zipf expts.mdb")
Set tblAuthNSC = CHZipf.OpenRecordset("tblAuthorNSCHistogram")
Set tblAuthNSC_nonMCA = CHZipf.OpenRecordset("tblAuthorNSCHistogram_nonMCA")
Set tblAuthNSC_noMegaRev = CHZipf.OpenRecordset("tblAuthorNSCHistogram_noMegaRev")
Set tblAuthSC = CHZipf.OpenRecordset("tblAuthorSCHistogram")
Set tblAuthNSCZipf = CHZipf.OpenRecordset("tblAuthorNSCZipfSlopes")
Set tblAuthSCZipf = CHZipf.OpenRecordset("tblAuthorSCZipfSlopes")
Set tblAuthOverallZipf = CHZipf.OpenRecordset("tblAuthorOverallZipfSlopes")
FileNum1 = FreeFile
Open "E:\Citation Hacking\Author\H-indexData.txt" For Output As FileNum1
Print #FileNum1, "AuthorName" & vbTab & "PMID" & vbTab & "Anchor" & vbTab & "Total_Cites" & vbTab & "Total_SC" & vbTab & "Total_NSC"

tblAuthList.MoveFirst
While Not tblAuthList.EOF
    tblTemp.Edit
    tblTemp!authorname = tblAuthList!Author
    AuthName$ = tblAuthList!Author
    tblTemp.Update
    qryAuthNSC.Requery
    If qryAuthNSC.RecordCount > 1 Then
        ProcessFlag = 0: MaxNumCites = 0: MaxFlag = 1
        qryAuthNSC.MoveFirst
        PrevFreq = qryAuthNSC!Freq
        While Not qryAuthNSC.EOF
            NumCites = qryAuthNSC!NumCites      'X
            Freq = qryAuthNSC!Freq
            LogFreq = Log10(Freq)               'Y
            tblAuthNSC.AddNew
            tblAuthNSC!Author = AuthName$
            tblAuthNSC!NSC = NumCites
            tblAuthNSC!NSCFreq = Freq
            tblAuthNSC.Update
            qryAuthNSC.MoveNext
        Wend
    End If

    qryAuthNSC_nonMCA.Requery                     'Tally NSC histogram for all authors, but take out their Most Citing Author (MCA)
    If qryAuthNSC_nonMCA.RecordCount > 1 Then
        qryAuthNSC_nonMCA.MoveFirst
        While Not qryAuthNSC_nonMCA.EOF
            tblAuthNSC_nonMCA.AddNew
            tblAuthNSC_nonMCA!Author = AuthName$
            tblAuthNSC_nonMCA!NSC = qryAuthNSC_nonMCA!NumCites
            tblAuthNSC_nonMCA!NSCFreq = qryAuthNSC_nonMCA!Freq
            tblAuthNSC_nonMCA.Update
            qryAuthNSC_nonMCA.MoveNext
        Wend
    End If
    
    qryAuthNSC_noMegaRev.Requery                   'Tally NSC histogram for all authors, but take out "mega-reviews" (currently def as >150 total cites)
    If qryAuthNSC_noMegaRev.RecordCount > 1 Then
        qryAuthNSC_noMegaRev.MoveFirst
        While Not qryAuthNSC_noMegaRev.EOF
            tblAuthNSC_noMegaRev.AddNew
            tblAuthNSC_noMegaRev!Author = AuthName$
            tblAuthNSC_noMegaRev!NSC = qryAuthNSC_noMegaRev!NumCites
            tblAuthNSC_noMegaRev!NSCFreq = qryAuthNSC_noMegaRev!Freq
            tblAuthNSC_noMegaRev.Update
            qryAuthNSC_noMegaRev.MoveNext
        Wend
    End If

    'self-cites
    qryAuthSC.Requery
    If qryAuthSC.RecordCount > 1 Then
        ProcessFlag = 0:  MaxNumCites = 0: MaxFlag = 1
        qryAuthSC.MoveFirst
        While Not qryAuthSC.EOF
            NumCites = qryAuthSC!NumSC          'X
            Freq = qryAuthSC!FreqSC
            LogFreq = Log10(Freq)               'Y
            tblAuthSC.AddNew
            tblAuthSC!Author = AuthName$
            tblAuthSC!sc = NumCites
            tblAuthSC!scfreq = Freq
            tblAuthSC.Update
            qryAuthSC.MoveNext
        Wend
    End If
    
    'Overall cites per author's paper (traditional H-index)
    Set qryHindex = CiteHack.OpenRecordset("SELECT * FROM qryAuthorHindex ORDER BY Total_Cites DESC")
    Hindex = 0
    If qryHindex.RecordCount > 0 Then
        qryHindex.MoveFirst
        While Not qryHindex.EOF
            If qryHindex!Total_cites > Hindex Then
                Hindex = Hindex + 1
            End If
            Print #FileNum1, qryHindex!authorname & vbTab & qryHindex!PMID & vbTab & qryHindex!Anchor & vbTab & qryHindex!Total_cites & vbTab & qryHindex!Total_SC & vbTab & qryHindex!Total_NSC
            qryHindex.MoveNext
        Wend
    End If
    'SC per author's paper (traditional H-index, SC only)
    Set qryHindex = CiteHack.OpenRecordset("SELECT * FROM qryAuthorHindex ORDER BY Total_SC DESC")
    Hindex_SC = 0
    If qryHindex.RecordCount > 0 Then
        qryHindex.MoveFirst
        While Not qryHindex.EOF
            If qryHindex!Total_SC > Hindex_SC Then
                Hindex_SC = Hindex_SC + 1
            Else
                qryHindex.MoveLast
            End If
            qryHindex.MoveNext
        Wend
    End If
    'NSC per author's paper (traditional H-index, NSC only)
    Set qryHindex = CiteHack.OpenRecordset("qryAuthorHindex_NSC")     'have to use seperate query since NSC=TotCites-SC (i.e., is calculated on the fly)
    Hindex_NSC = 0
    If qryHindex.RecordCount > 0 Then
        qryHindex.MoveFirst
        While Not qryHindex.EOF
            If qryHindex!Total_NSC > Hindex_NSC Then
                Hindex_NSC = Hindex_NSC + 1
            Else
                qryHindex.MoveLast
            End If
            qryHindex.MoveNext
        Wend
    End If
        
    tblAuthOverallZipf.AddNew
    tblAuthOverallZipf!Author = AuthName$
    If Hindex > 0 Then tblAuthOverallZipf!All_Hindex = Hindex
    If Hindex_NSC > 0 Then tblAuthOverallZipf!NSC_Hindex = Hindex_NSC
    If Hindex_SC > 0 Then tblAuthOverallZipf!SC_Hindex = Hindex_SC
    'calc the % of outgoing citations that are to the author's own papers
    qrySCpct.Requery
    If qrySCpct.RecordCount > 0 Then
        qrySCpct.MoveFirst
        tblAuthOverallZipf!TotSC = qrySCpct!SumOfSelf_refs
        If qrySCpct!SumOfTotal_refs >= 100 Then      'to avoid statistics of small #s
            tblAuthOverallZipf!overallSCpct = Int((qrySCpct!SumOfSelf_refs / qrySCpct!SumOfTotal_refs) * 100) / 100
            'At least 10 anchor papers with references and at least 100 total references
            If qrySCpct!SumOfAnchor_refs >= 100 And qrySCpct!CountOfAnchor_refs >= 9 Then
                tblAuthOverallZipf!anchorSCpct = Int((qrySCpct!SumOfAnchor_self_refs / qrySCpct!SumOfAnchor_refs) * 100) / 100
            Else
                tblAuthOverallZipf!anchorSCpct = Null
            End If
            tblAuthOverallZipf!NonanchorSCpct = Int(((qrySCpct!SumOfSelf_refs - qrySCpct!SumOfAnchor_self_refs) / (qrySCpct!SumOfTotal_refs - qrySCpct!SumOfAnchor_refs + 1)) * 100) / 100
        End If
    End If
    tblAuthOverallZipf.Update
    
    tblAuthList.MoveNext
    AuthRecs = AuthRecs + 1
    lblProgress.Caption = Str(AuthRecs) & " authors processed": lblProgress.Refresh
Wend
Close FileNum1
frmProcessing.MousePointer = vbDefault
End Sub
'Calculate Log to the Base 10
Function Log10(X)
   Log10 = Log(X) / Log(10#)
End Function
