Attribute VB_Name = "TextPreprocessing"
Public Sub Clean_Text(b$)
'----------------------------------------------------------------------------------------------------
' This routine pre-processes text that will be fed into all other routines. Currently, a lot of it
'      is encoding conversion problems (e.g., UTF-8, Windows-1252 & XML to ASCII). But also to reduce variation
'      in specific equations of interest (e.g., change "p less than" to "p<")
'----------------------------------------------------------------------------------------------------
    If InStr(b$, vbTab) > 0 Then b$ = Replace(b$, vbTab, " ")
    If InStr(b$, "&lt;") > 0 Then b$ = Replace(b$, "&lt;", "<")
    If InStr(b$, "&gt;") > 0 Then b$ = Replace(b$, "&gt;", ">")
    If InStr(b$, "&amp;") > 0 Then b$ = Replace(b$, "&amp;", "&")
    If InStr(b$, "&ndash;") > 0 Then b$ = Replace(b$, "&ndash;", "-")
    If InStr(b$, "&mdash;") > 0 Then b$ = Replace(b$, "&mdash;", "-")
    If InStr(b$, "&tilde;") > 0 Then b$ = Replace(b$, "&tilde;", "~")
    If InStr(b$, "&quot;") > 0 Then b$ = Replace(b$, "&quot;", vbNullString)    'don't need quotes
    If InStr(b$, "ˆ") > 0 Then b$ = Replace(b$, "ˆ", "^")
    If InStr(b$, "²") > 0 Then b$ = Replace(b$, "²", "^2")
    If InStr(b$, "³") > 0 Then b$ = Replace(b$, "³", "^3")
    If InStr(b$, "¼") > 0 Then b$ = Replace(b$, "¼", "1/4")
    If InStr(b$, "½") > 0 Then b$ = Replace(b$, "½", "1/2")
    If InStr(b$, "¾") > 0 Then b$ = Replace(b$, "¾", "3/4")
    If InStr(b$, "µ") > 0 Then b$ = Replace(b$, "µ", "�")
    If InStr(b$, "’") > 0 Then b$ = Replace(b$, "’", "`")
    If InStr(b$, "˜") > 0 Then b$ = Replace(b$, "˜", "~")
    If InStr(b$, "÷") > 0 Then b$ = Replace(b$, "÷", "/")
    If InStr(b$, "�") > 0 Then b$ = Replace(b$, "�", ".")
    If InStr(b$, "·") > 0 Then b$ = Replace(b$, "·", ".")
    If InStr(b$, "∼") > 0 Then b$ = Replace(b$, "∼", "-")
    If InStr(b$, "‒") > 0 Then b$ = Replace(b$, "‒", "-")
    If InStr(b$, "–") > 0 Then b$ = Replace(b$, "–", "-")
    If InStr(b$, "—") > 0 Then b$ = Replace(b$, "—", "-")
    If InStr(b$, "≤") > 0 Then b$ = Replace(b$, "≤", "<=")
    If InStr(b$, "≥") > 0 Then b$ = Replace(b$, "≥", ">=")
    If InStr(b$, "±") > 0 Then b$ = Replace(b$, "±", "+/-")
    If InStr(b$, "×") > 0 Then b$ = Replace(b$, "×", "x")
    If InStr(b$, " ") > 0 Then b$ = Replace(b$, " ", " ")
    If InStr(b$, "α") > 0 Then b$ = Replace(b$, "α", "alpha")
    If InStr(b$, "β") > 0 Then b$ = Replace(b$, "β", "beta")
    If InStr(b$, "ß") > 0 Then b$ = Replace(b$, "ß", "beta")
    If InStr(b$, "Δ") > 0 Then b$ = Replace(b$, "Δ", "delta")
    If InStr(b$, "γ") > 0 Then b$ = Replace(b$, "γ", "gamma")
    If InStr(b$, " ") > 0 Then b$ = Replace(b$, " ", vbNullString)
    If InStr(b$, " ") > 0 Then b$ = Replace(b$, " ", vbNullString)
    If InStr(b$, " ") > 0 Then b$ = Replace(b$, " ", vbNullString)
    If InStr(b$, " ") > 0 Then b$ = Replace(b$, " ", vbNullString)
    If InStr(b$, "(®)") > 0 Then b$ = Replace(b$, "(®)", vbNullString)        'copyright symbol
    If InStr(b$, "“") > 0 Then b$ = Replace(b$, "“", vbNullString)          'double quote
    If InStr(b$, Chr$(34)) > 0 Then b$ = Replace(b$, Chr$(34), vbNullString)    'double quote
    If InStr(b$, "'") > 0 Then b$ = Replace(b$, "'", "`")       'single quote is forbidden in MS Access database queries

    'XML conversion problems
    If InStr(b$, "&#x0") > 0 Then           'if no root XML char patterns, then save time by not doing all these checks
        If InStr(b$, "&#x000a0;") > 0 Then b$ = Replace(b$, "&#x000a0;", " ")
        If InStr(b$, "&#x000b1;") > 0 Then b$ = Replace(b$, "&#x000b1;", "+/-")
        If InStr(b$, "&#x000b2;") > 0 Then b$ = Replace(b$, "&#x000b2;", "^2")
        If InStr(b$, "&#x000b3;") > 0 Then b$ = Replace(b$, "&#x000b3;", "^3")
        If InStr(b$, "&#x000b5;") > 0 Then b$ = Replace(b$, "&#x000b5;", "�")
        If InStr(b$, "&#x000b7;") > 0 Then b$ = Replace(b$, "&#x000b7;", ".")
        If InStr(b$, "&#x000d7;") > 0 Then b$ = Replace(b$, "&#x000d7;", "x")
        If InStr(b$, "&#x0002a;") > 0 Then b$ = Replace(b$, "&#x0002a;", "*")
        If InStr(b$, "&#x0002b;") > 0 Then b$ = Replace(b$, "&#x0002b;", "+")
        If InStr(b$, "&#x0002c;") > 0 Then b$ = Replace(b$, "&#x0002c;", ",")
        If InStr(b$, "&#x0002e;") > 0 Then b$ = Replace(b$, "&#x0002e;", ".")
        If InStr(b$, "&#x00022;") > 0 Then b$ = Replace(b$, "&#x00022;", vbNullString)  'quote
        If InStr(b$, "&#x00025;") > 0 Then b$ = Replace(b$, "&#x00025;", "%")
        If InStr(b$, "&#x00026;") > 0 Then b$ = Replace(b$, "&#x00026;", "&")
        If InStr(b$, "&#x0003c;") > 0 Then b$ = Replace(b$, "&#x0003c;", "<")
        If InStr(b$, "&#x0003d;") > 0 Then b$ = Replace(b$, "&#x0003d;", "=")
        If InStr(b$, "&#x0003e;") > 0 Then b$ = Replace(b$, "&#x0003e;", ">")
        If InStr(b$, "&#x0003b;") > 0 Then b$ = Replace(b$, "&#x0003b;", ";")
        If InStr(b$, "&#x0003a;") > 0 Then b$ = Replace(b$, "&#x0003a;", ":")
        If InStr(b$, "&#x0005b;") > 0 Then b$ = Replace(b$, "&#x0005b;", "(")
        If InStr(b$, "&#x0005d;") > 0 Then b$ = Replace(b$, "&#x0005d;", ")")
        If InStr(b$, "&#x003b1;") > 0 Then b$ = Replace(b$, "&#x003b1;", "alpha")
        If InStr(b$, "&#x003b2;") > 0 Then b$ = Replace(b$, "&#x003b2;", "beta")
        If InStr(b$, "&#x003b3;") > 0 Then b$ = Replace(b$, "&#x003b3;", "gamma")
        If InStr(b$, "&#x003b4;") > 0 Then b$ = Replace(b$, "&#x003b4;", "delta")
        If InStr(b$, "&#x003bc;") > 0 Then b$ = Replace(b$, "&#x003bc;", "�")
        If InStr(b$, "&#x003c7;") > 0 Then b$ = Replace(b$, "&#x003c7;", "chi")
        If InStr(b$, "&#x02003;") > 0 Then b$ = Replace(b$, "&#x02003;", " ")
        If InStr(b$, "&#x02005;") > 0 Then b$ = Replace(b$, "&#x02005;", " ")
        If InStr(b$, "&#x02008;") > 0 Then b$ = Replace(b$, "&#x02008;", " ")
        If InStr(b$, "&#x02009;") > 0 Then b$ = Replace(b$, "&#x02009;", " ")
        If InStr(b$, "&#x0200a;") > 0 Then b$ = Replace(b$, "&#x0200a;", " ")
        If InStr(b$, "&#x02010;") > 0 Then b$ = Replace(b$, "&#x02010;", "-")
        If InStr(b$, "&#x02013;") > 0 Then b$ = Replace(b$, "&#x02013;", "-")
        If InStr(b$, "&#x02014;") > 0 Then b$ = Replace(b$, "&#x02014;", "-")
        If InStr(b$, "&#x02019;") > 0 Then b$ = Replace(b$, "&#x02019;", vbNullString)  'quote
        If InStr(b$, "&#x0201c;") > 0 Then b$ = Replace(b$, "&#x0201c;", vbNullString)  'quote
        If InStr(b$, "&#x02020;") > 0 Then b$ = Replace(b$, "&#x02019;", vbNullString)  'bullet
        If InStr(b$, "&#x02021;") > 0 Then b$ = Replace(b$, "&#x02019;", vbNullString)  'dagger
        If InStr(b$, "&#x02044;") > 0 Then b$ = Replace(b$, "&#x02044;", "/")
        If InStr(b$, "&#x02212;") > 0 Then b$ = Replace(b$, "&#x02212;", "-")
        If InStr(b$, "&#x02215;") > 0 Then b$ = Replace(b$, "&#x02215;", "/")
        If InStr(b$, "&#x02217;") > 0 Then b$ = Replace(b$, "&#x02217;", "*")
        If InStr(b$, "&#x0223c;") > 0 Then b$ = Replace(b$, "&#x0223c;", "~")
        If InStr(b$, "&#x02264;") > 0 Then b$ = Replace(b$, "&#x02264;", "<=")
        If InStr(b$, "&#x02265;") > 0 Then b$ = Replace(b$, "&#x02265;", ">=")
        If InStr(b$, "&#x02a7d;") > 0 Then b$ = Replace(b$, "&#x02a7d;", "<=")
        If InStr(b$, "&#x02a7e;") > 0 Then b$ = Replace(b$, "&#x02a7e;", ">=")
    End If
    
    'increase standardization of text
    If InStr(b$, vbCr) > 0 Then b$ = Replace(b$, vbCr, " ")             'convert linefeeds/CRs to spaces
    If InStr(b$, vbLf) > 0 Then b$ = Replace(b$, vbLf, " ")
    If InStr(b$, "{") > 0 Then b$ = Replace(b$, "{", "(")               'standardize parentheticals (nested parens are helpful for human readbility, but not algorithmic processing)
    If InStr(b$, "}") > 0 Then b$ = Replace(b$, "}", ")")
    If InStr(b$, "[") > 0 Then b$ = Replace(b$, "[", "(")
    If InStr(b$, "]") > 0 Then b$ = Replace(b$, "]", ")")
    If InStr(b$, "(") > 0 Then b$ = Replace(b$, "(", " (")              'add space before paren (to ensure the parenthetical is detected as a separate word)
    If InStr(b$, "  (") > 0 Then b$ = Replace(b$, "  (", " (")          'remove double space if one was introduced by previous step
    If InStr(b$, "( ") > 0 Then b$ = Replace(b$, "( ", "(")             'eliminate unnecessary spaces between critical parsing symbols
    If InStr(b$, " )") > 0 Then b$ = Replace(b$, " )", ")")
    If InStr(b$, "+/- ") > 0 Then b$ = Replace(b$, "+/- ", "+/-")
    If InStr(b$, " +/-") > 0 Then b$ = Replace(b$, " +/-", "+/-")
    If InStr(b$, "less than") > 0 Then b$ = Replace(b$, "less than", "<")
    If InStr(b$, "greater than") > 0 Then b$ = Replace(b$, "greater than", ">")
    If InStr(b$, "equal to") > 0 Then b$ = Replace(b$, "equal to", "=")
    If InStr(b$, "equals") > 0 Then b$ = Replace(b$, "equals", "=")
    If InStr(b$, "= ") > 0 Then b$ = Replace(b$, "= ", "=")
    If InStr(b$, " =") > 0 Then b$ = Replace(b$, " =", "=")
    If InStr(b$, "< ") > 0 Then b$ = Replace(b$, "< ", "<")
    If InStr(b$, " <") > 0 Then b$ = Replace(b$, " <", "<")
    If InStr(b$, "> ") > 0 Then b$ = Replace(b$, "> ", ">")
    If InStr(b$, " >") > 0 Then b$ = Replace(b$, " >", ">")
    If InStr(b$, "<or=") > 0 Then b$ = Replace(b$, "<or=", "<=")
    If InStr(b$, "=or<") > 0 Then b$ = Replace(b$, "=or<", "<=")
    If InStr(b$, ">or=") > 0 Then b$ = Replace(b$, ">or=", ">=")
    If InStr(b$, "=or>") > 0 Then b$ = Replace(b$, "=or>", ">=")
    If InStr(b$, ", ,") > 0 Then b$ = Replace(b$, ", ,", ",")
    If InStr(b$, " per cent") > 0 Then b$ = Replace(b$, " per cent", "% ")
    If InStr(b$, " percent ") > 0 Then b$ = Replace(b$, " percent ", "% ")
    If InStr(b$, "Adj") > 0 Then b$ = Replace(b$, "Adj", "adj")
    If InStr(b$, "adj. ") > 0 Then b$ = Replace(b$, "adj. ", "adj ")
    If InStr(b$, " vs. ") > 0 Then b$ = Replace(b$, " vs. ", " vs ")
    If InStr(b$, " v. ") > 0 Then b$ = Replace(b$, " v. ", " vs ")
    If InStr(b$, " versus ") > 0 Then b$ = Replace(b$, " versus ", " vs ")
    
    'convert any double spaces (introduced by preprocessing or natural) to single spaces
    While InStr(b$, "  ") > 0
        b$ = Replace(b$, "  ", " ")
    Wend
End Sub

Public Sub StripOutXML(a$)
'----------------------------------------------------------------------------------------------------
' This routine is sort of a hackish way to strip out XML tags. There is probably a better way,
'      but this will suffice for now. It looks for paired tags and removes both if it finds them
'----------------------------------------------------------------------------------------------------
Dim t&, u&, v&, TagPos&, NextTagPos&
Dim Alen&, UB&
Dim QuitFlag%
Dim sp$(), sp2$()
Dim LeftTag$, RightTag$, TagCleanup$
Dim PMIDlabel$, PMCIDlabel$, Jlabel$, ArticleTypeLabel$

If InStr(a$, "<ref-list") > 0 Then a$ = Replace(a$, "<ref-list", "\ref-list\") ' to mark (and keep) the point where references begin. Only the URL routine needs the refs

PMIDlabel$ = "<article-id pub-id-type=" & Chr$(34) & "pmid" & Chr$(34) & ">"
PMCIDlabel$ = "<article-id pub-id-type=" & Chr$(34) & "pmc" & Chr$(34) & ">"
Jlabel$ = "<journal-id journal-id-type=" & Chr$(34) & "nlm-ta" & Chr$(34) & ">"
ArticleTypeLabel$ = "<subject>"
If InStr(a$, PMIDlabel$) > 0 Then
    sp$ = Split(a$, PMIDlabel$)
    sp2$ = Split(sp$(1), "</article-id>")
    PMID = sp2$(0)
Else
    PMID = 0
End If
If InStr(a$, PMCIDlabel$) > 0 Then
    sp$ = Split(a$, PMCIDlabel$)
    sp2$ = Split(sp$(1), "</article-id>")
    PMCID = sp2$(0)
Else
    PMCID = 0
End If
If InStr(a$, Jlabel$) > 0 Then
    sp$ = Split(a$, Jlabel$)
    sp2$ = Split(sp$(1), "</journal-id>")
    JName$ = sp2$(0)
Else
    JName$ = vbNullString
End If
If InStr(a$, ArticleTypeLabel$) > 0 Then
    sp$ = Split(a$, ArticleTypeLabel$)
    sp2$ = Split(sp$(1), "</subject>")
    ArticleType$ = sp2$(0)
Else
    ArticleType$ = vbNullString
End If

If InStr(a$, ".</p>") > 0 Then a$ = Replace(a$, ".</p>", ". ")
If InStr(a$, "</p>") > 0 Then a$ = Replace(a$, "</p>", ". ")
If InStr(a$, "<p>") > 0 Then a$ = Replace(a$, "<p>", " ")
If InStr(a$, " <hr/>") > 0 Then a$ = Replace(a$, " <hr/>", ". ")
If InStr(a$, "<hr/><") > 0 Then a$ = Replace(a$, "<hr/><", ". ")
If InStr(a$, "<hr/>") > 0 Then a$ = Replace(a$, "<hr/>", ".")
If InStr(a$, "<graphic xlink:href=") > 0 Then a$ = Replace(a$, "<graphic xlink:href=", " ")           'hackish solution, but it's messing things up
If InStr(a$, "<break/>") > 0 Then a$ = Replace(a$, "<break/>", " ")      'table formatting tag
If InStr(a$, "<td/>") > 0 Then a$ = Replace(a$, "<td/>", ". ")           'table formatting tag
If InStr(a$, "<table ") > 0 Then a$ = Replace(a$, "<table ", " \tablestart\ <table ")       'mark beginning of tables
If InStr(a$, "</table>") > 0 Then a$ = Replace(a$, "</table>", " \tableend\ </table>")      'mark end of tables

Alen = Len(a$)
t = 1: TagCleanup = vbNullString
While t <= Alen
    If Mid$(a$, t, 1) = "<" And Mid$(a$, t + 1, 1) <> "/" Then      'this is an opening (left) XML tag
        u = 0: QuitFlag = 0
        While QuitFlag = 0
            d$ = Mid$(a$, t + u, 1)
            If d$ = ">" Then                        'simple pattern: <field> </field>
                LeftTag$ = Mid$(a$, t, u + 1)
                RightTag$ = Replace(LeftTag$, "<", "</")
                If InStr(a$, RightTag$) > 0 Then
                    a$ = Replace(a$, LeftTag$, " ")
                    If InStr(TagCleanup, RightTag$) = 0 Then        'can't delete the right tags until all the left ones are gone (otherwise there are no further left-right matches)
                        TagCleanup = TagCleanup & RightTag$ & "|"
                    End If
                    Alen = Len(a$)                  'account for new length
                    t = t - Len(LeftTag$)
                    If t < 1 Then t = 1
                    QuitFlag = 1
                End If
            End If
            
            If d$ = " " Then
                LeftTag$ = Mid$(a$, t, u) & ">"
                RightTag$ = Replace(LeftTag$, "<", "</")
                If InStr(a$, RightTag$) > 0 Then                  'more complicated pattern: <field blah blah blah> </field>
                    TagPos = InStr(t + u - 1, a$, ">")            'search for next delimiter
                    If TagPos > 0 Then
                        LeftTag$ = Mid$(a$, t, TagPos - t + 1)
                        a$ = Replace(a$, LeftTag$, " ")
                    End If
                    If InStr(TagCleanup, RightTag$) = 0 Then        'can't delete the right tags until all the left ones are gone (otherwise there are no further left-right matches)
                        TagCleanup = TagCleanup & RightTag$ & "|"
                    End If
                    Alen = Len(a$)
                    t = t - Len(LeftTag$)
                    If t < 1 Then t = 1
                    QuitFlag = 1
                End If
            End If
            u = u + 1
            If u > 50 Then QuitFlag = 1         'limit to 50 chars long
        Wend
    End If
    t = t + 1
Wend
If Right$(TagCleanup$, 1) = "|" Then TagCleanup$ = Left$(TagCleanup$, Len(TagCleanup$) - 1)
sp$ = Split(TagCleanup$, "|")
UB = UBound(sp$)
For t = 1 To UB                                 'cleanup all the right tags
    a$ = Replace(a$, sp$(t), " ")
Next t
If InStr(a$, "  ") > 0 Then a$ = Replace(a$, "  ", " ")     'clean up all double spaces
End Sub

Public Sub Convert_Month(mon$)
If Len(mon$) = 7 Then           'Range (e.g. 1992 Jan-Feb)
    If Mid$(mon$, 4, 1) = "-" Then mon$ = Left$(mon$, 3)
End If
Select Case mon$
Case "Jan": mon$ = "01"
Case "Feb": mon$ = "02"
Case "Mar": mon$ = "03"
Case "Apr": mon$ = "04"
Case "May": mon$ = "05"
Case "Jun": mon$ = "06"
Case "Jul": mon$ = "07"
Case "Aug": mon$ = "08"
Case "Sep": mon$ = "09"
Case "Oct": mon$ = "10"
Case "Nov": mon$ = "11"
Case "Dec": mon$ = "12"
Case Else
    mon$ = "unknown"
End Select
End Sub

